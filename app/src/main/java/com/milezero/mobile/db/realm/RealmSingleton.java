package com.milezero.mobile.db.realm;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.exceptions.RealmMigrationNeededException;

/**
 * Created by Suraj Dubey on 13/12/15.
 */
public class RealmSingleton {

    private static String REAL_DB_NAME = "milezero1.realm";

    public static Realm getInstance(Context context) {
        RealmConfiguration configuration = new RealmConfiguration.Builder(context).name(REAL_DB_NAME).build();
        try {
            return Realm.getInstance(configuration);
        }

        catch (RealmMigrationNeededException e) {
            try {
                Realm.deleteRealm(configuration);
                return Realm.getInstance(configuration);
            }
            catch (Exception ex) {
                throw ex;
            }
        }
    }
}
