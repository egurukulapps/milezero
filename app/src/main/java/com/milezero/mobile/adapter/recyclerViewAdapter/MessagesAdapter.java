package com.milezero.mobile.adapter.recyclerViewAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.milezero.mobile.R;
import com.milezero.mobile.model.messages.Messages;

import java.util.List;

/**
 * Created by nishant on 26/11/15.
 */
public class MessagesAdapter extends BaseAdapter {

    private Context context;
    private List<Messages> messagesItems;


    public MessagesAdapter(Context context, List<Messages> navDrawerItems) {
        this.context = context;
        this.messagesItems = navDrawerItems;
    }

    @Override
    public int getCount() {
        return messagesItems.size();
    }

    @Override
    public Object getItem(int position) {
        return messagesItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        /**
         * The following list not implemented reusable list items as list items
         * are showing incorrect data Add the solution if you have one
         * */

        final Messages m = messagesItems.get(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        // Identifying the message owner

        if (messagesItems.get(position).isSelf()) {
            // message belongs to you, so load the right aligned layout
            convertView = mInflater.inflate(R.layout.item_chat_right,
                    null);
        } else {
            // message belongs to other person, load the left aligned layout
            convertView = mInflater.inflate(R.layout.item_chat_left,
                    null);
        }

        TextView txtMsg = (TextView) convertView.findViewById(R.id.txtMsg);
        TextView time = (TextView) convertView.findViewById(R.id.time);
        if (m.getTime() != null) {
            time.setText(m.getTime());
        } else {
            time.setText("");
        }

        txtMsg.setText(m.getMessage());


        return convertView;
    }
}
