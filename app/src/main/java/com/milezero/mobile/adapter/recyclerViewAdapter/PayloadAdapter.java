package com.milezero.mobile.adapter.recyclerViewAdapter;

/**
 * Created by nishant on 7/1/16.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.milezero.mobile.R;
import com.milezero.mobile.activities.RouteDetailActivity;
import com.milezero.mobile.interfaces.OnItemClickListener;
import com.milezero.mobile.model.driveritinerary.db.Payload;
import com.milezero.mobile.utils.L;

import io.realm.RealmList;

/**
 * Created by suraj on 10/12/15.
 */
public class PayloadAdapter extends RecyclerView.Adapter<PayloadAdapter.PayLoadViewHolder> {

    Context mContext;
    RealmList<Payload> payloads;

   public OnItemClickListener onItemClickListener;


    public PayloadAdapter(RealmList<Payload> payloads, Context context, OnItemClickListener onItemClickListener) {
        this.payloads = payloads;
        this.onItemClickListener = onItemClickListener;
        this.mContext = context;
    }

    @Override
    public PayLoadViewHolder onCreateViewHolder(ViewGroup parentViewGroup, int viewType) {

        View itemView = LayoutInflater
                .from(parentViewGroup.getContext())
                .inflate(R.layout.item_packages, parentViewGroup, false);
        PayLoadViewHolder holder = new PayLoadViewHolder(itemView);

        return holder;

    }

    @Override
    public void onBindViewHolder(final PayLoadViewHolder viewHolder, final int position) {
        Payload payload = payloads.get(position);

        if (payload != null) {

            if (payload.getItem_name() != null) {


                viewHolder.tvName.setText(payload.getItem_name());
            }

            if (payload.getItem_number() != null) {
                viewHolder.tvNumber.setText(payload.getItem_number());
            }

            if (payload.getItem_dimension() != null) {
                L.e(payload.getItem_dimension());
                viewHolder.tvDimension.setText(payload.getItem_dimension());
            }
        }


    }


    @Override
    public int getItemCount() {

        return payloads.size();

    }

    private void startRouteDetailActivity(int currentPosition) {

        Intent intent = new Intent(mContext, RouteDetailActivity.class);

        intent.putExtra("currentPosition", currentPosition);


        mContext.startActivity(intent);
    }

    public  class PayLoadViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView tvDimension;
        TextView tvNumber;
        TextView tvName;


        public PayLoadViewHolder(View itemView) {
            super(itemView);


            tvDimension = (TextView) itemView.findViewById(R.id.tvDimension);
            tvNumber = (TextView) itemView.findViewById(R.id.tvNumber);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {

            onItemClickListener.onItemClick(v, getAdapterPosition());
        }
    }


}

