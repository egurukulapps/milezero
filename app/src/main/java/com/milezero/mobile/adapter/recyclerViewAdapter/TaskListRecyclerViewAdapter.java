package com.milezero.mobile.adapter.recyclerViewAdapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.milezero.mobile.R;
import com.milezero.mobile.activities.RouteDetailActivity;
import com.milezero.mobile.model.driveritinerary.db.DriverItinerary;
import com.milezero.mobile.model.driveritinerary.db.Stop;
import com.milezero.mobile.model.driveritinerary.db.Task;

import java.util.ArrayList;

import io.realm.RealmList;

/**
 * Created by suraj on 10/12/15.
 */
public class TaskListRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    RealmList<Stop> mStops;

    ArrayList<Integer> numberOfTaskInStop;

    int currentStopPosition = 0;
    int currentTaskPosition = 0;

    int currentStepNumber = 0;
//    ArrayList<TaskData> taskList;

    DriverItinerary mDriverItinerary;

    public TaskListRecyclerViewAdapter(DriverItinerary driverItinerary, RealmList<Stop> stops, Context context) {
        this.mStops = stops;
        this.mContext = context;
        this.mDriverItinerary = driverItinerary;

//        prepareTaskData();
        setNumberOfTaskInStop();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parentViewGroup, int viewType) {

        switch (viewType) {


            case 0:

                View view = LayoutInflater
                        .from(parentViewGroup.getContext())
                        .inflate(R.layout.item_slots, parentViewGroup, false);
                TaskViewHolderSlots holderSlots = new TaskViewHolderSlots(view);


                return holderSlots;

            case 1:


                View itemView = LayoutInflater
                        .from(parentViewGroup.getContext())
                        .inflate(R.layout.item_stops_list_past, parentViewGroup, false);
                TaskViewHolder holder = new TaskViewHolder(itemView);

                return holder;


            case 2:


                View itemView2 = LayoutInflater
                        .from(parentViewGroup.getContext())
                        .inflate(R.layout.item_stops_list_present, parentViewGroup, false);
                TaskViewHolder holder2 = new TaskViewHolder(itemView2);

                return holder2;

            case 3:


                View itemView3 = LayoutInflater
                        .from(parentViewGroup.getContext())
                        .inflate(R.layout.item_stops_list_future, parentViewGroup, false);
                TaskViewHolder holder3 = new TaskViewHolder(itemView3);

                return holder3;


            default:
                View itemDefault = LayoutInflater
                        .from(parentViewGroup.getContext())
                        .inflate(R.layout.card_view_task_list_single_row, parentViewGroup, false);
                TaskViewHolder holderDefault = new TaskViewHolder(itemDefault);

                return holderDefault;


        }

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {
        Stop stopAtCurrentPosition = mStops.get(position);

        if (viewHolder.getItemViewType() == 0) {

            TaskViewHolderSlots holderSlots = (TaskViewHolderSlots) viewHolder;

            holderSlots.slot.setText(stopAtCurrentPosition.getTime());

        } else {

            final TaskViewHolder holder = (TaskViewHolder) viewHolder;


//        TaskData taskData = taskList.get(position);
            Task taskData = stopAtCurrentPosition.getTasks().get(0);


//        final Task task = taskData.getTask();
//        currentStopPosition = taskData.getStopId();



            currentStepNumber++;
            holder.tvStepNumber.setText(stopAtCurrentPosition.getStopNumber() + "");
            holder.tvType.setText(taskData.getTaskName());
            String addressString = stopAtCurrentPosition.getAddress().getCity() + "\n" + stopAtCurrentPosition.getAddress().getStreet() + "\n" + stopAtCurrentPosition.getAddress().getAddressType();

            holder.tvAddress.setText(addressString);


            holder.taskIndex = currentTaskPosition;
            holder.stopIndex = currentStopPosition;


            holder.layoutTask.setTag(position);
            holder.layoutTask.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();


                    startRouteDetailActivity(holder.taskIndex, holder.stopIndex, position);
                }
            });
        }


    }

    @Override
    public int getItemViewType(int position) {

        if (mStops.get(position).getType() == 0) {
            return mStops.get(position).getType();
        } else {

            String status = mStops.get(position).getStatus();

            if (status != null) {
                switch (status) {

                    case "PAST":
                        return 1;

                    case "PRESENT":
                        return 2;

                    case "FUTURE":
                        return 3;

                    default:
                        return 3;


                }
            } else {
                return 3;
            }


        }


    }

    @Override
    public int getItemCount() {
//        int totalSize = 0;
        int stopSize = mStops.size();
//        for(int index = 0; index<stopSize; index++) {
//            totalSize = totalSize + mStops.get(index).getTasks().size();
//        }
//        L.d("Total Size: " + totalSize);
//        return totalSize;
        return stopSize;

    }

    private void startRouteDetailActivity(int taskIndex, int stopIndex, int currentPosition) {
        //get requested Task from position

//        Gson gson = new Gson();
//        Geocode geocode = mStops.get(stopIndex).getGeocode();

//        Task currentTask = mStops.get(stopIndex).getTasks().get(taskIndex);
//        Stop currentStop = mStops.get(stopIndex);

//        L.d(currentStop.getAddress().getCity());
        //display RouteDetailActivity
        Intent intent = new Intent(mContext, RouteDetailActivity.class);

        intent.putExtra("currentPosition", currentPosition);

//        TaskListSingleton.taskList = taskList;

        mContext.startActivity(intent);
    }

    public static class TaskViewHolder extends RecyclerView.ViewHolder {
        LinearLayout layoutTask;
        TextView tvActionTime;

        TextView tvStepNumber;
        TextView tvType;
        TextView tvAddress;

        int stopIndex = 0;
        int taskIndex = 0;

        public TaskViewHolder(View itemView) {
            super(itemView);

            layoutTask = (LinearLayout) itemView.findViewById(R.id.layoutTask);


            tvActionTime = (TextView) itemView.findViewById(R.id.tvActionTime);
            tvStepNumber = (TextView) itemView.findViewById(R.id.tvStepNumber);
            tvType = (TextView) itemView.findViewById(R.id.tvType);
            tvAddress = (TextView) itemView.findViewById(R.id.tvAddress);
//            tvCity = (TextView) itemView.findViewById(R.id.tvCity);
//            tvAddressType = (TextView) itemView.findViewById(R.id.tvAddressType);

        }
    }


    public static class TaskViewHolderSlots extends RecyclerView.ViewHolder {

        TextView slot;


        public TaskViewHolderSlots(View itemView) {
            super(itemView);


            slot = (TextView) itemView.findViewById(R.id.slot);


        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private void setNumberOfTaskInStop() {
//        int stopSize = mStops.size();
//        numberOfTaskInStop = new ArrayList<>();
//        for(int stopIndex=0;stopIndex<stopSize;stopIndex++)
//        {
//            int taskSize = mStops.get(stopIndex).getTasks().size();
//            numberOfTaskInStop.add(taskSize);
//        }
        mStops.size();
    }

    /*
        returns [stopIndex, taskIndex]
     */
    private int[] getStopTaskIndex(int requestedPosition) {
        int remainingTaskCount = requestedPosition + 1;
        int stopSize = mStops.size();

        int taskIndex = 0;
        int stopIndex = 0;

        for (stopIndex = 0; stopIndex < stopSize; stopIndex++) {
            int taskCountAtStop = mStops.get(stopIndex).getTasks().size();
            if ((remainingTaskCount - taskCountAtStop) >= 0) {
                remainingTaskCount = remainingTaskCount - taskCountAtStop;
            } else {
                break;
            }
        }

        if (remainingTaskCount == 0) {
            stopIndex--;
            taskIndex = mStops.get(stopIndex).getTasks().size() - 1;
        } else {
            taskIndex = remainingTaskCount;
        }


        return new int[]{stopIndex, taskIndex};
    }

}
