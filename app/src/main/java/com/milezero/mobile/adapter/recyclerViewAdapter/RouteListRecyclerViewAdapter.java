package com.milezero.mobile.adapter.recyclerViewAdapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.milezero.mobile.R;
import com.milezero.mobile.model.driveritinerary.db.DriverItinerary;
import com.milezero.mobile.model.temp.TaskData;

import java.util.ArrayList;

/**
 * Created by suraj on 10/12/15.
 */
public class RouteListRecyclerViewAdapter extends RecyclerView.Adapter<RouteListRecyclerViewAdapter.RouteViewHolder> {

    ArrayList<String> mSummaryList = new ArrayList<>();
    DriverItinerary mItinerary;
    Context mContext;

    public RouteListRecyclerViewAdapter(DriverItinerary itinerary, Context context) {
        this.mItinerary = itinerary;
        this.mContext = context;

        mSummaryList.add("Time Rang to be displayed");

    }

    @Override
    public RouteViewHolder onCreateViewHolder(ViewGroup parentViewGroup, int viewType) {
        View itemView = LayoutInflater
                .from(parentViewGroup.getContext())
                .inflate(R.layout.card_view_route_list_single_row, parentViewGroup, false);

        RouteViewHolder holder = new RouteViewHolder(itemView);

        return holder;
    }

    @Override
    public void onBindViewHolder(RouteViewHolder routeViewHolder, int position) {
        String route = mSummaryList.get(position);
        routeViewHolder.tvSummaryInformaation.setText(route);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);

        TaskListRecyclerViewAdapter adapter = new TaskListRecyclerViewAdapter(mItinerary, mItinerary.getStops(),mContext);

        routeViewHolder.recyclerViewTaskList.setLayoutManager(layoutManager);
        routeViewHolder.recyclerViewTaskList.setHasFixedSize(true);
        routeViewHolder.recyclerViewTaskList.setAdapter(adapter);

    }

    @Override
    public int getItemCount() {
        return mSummaryList.size();
    }

    public static class RouteViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvSummaryInformaation;
        RecyclerView recyclerViewTaskList;

        public RouteViewHolder(View itemView) {
            super(itemView);

            tvSummaryInformaation = (TextView) itemView.findViewById(R.id.tvSummaryInformation);
            recyclerViewTaskList = (RecyclerView) itemView.findViewById(R.id.recyclerViewTaskList);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}
