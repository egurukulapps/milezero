package com.milezero.mobile.api;

import com.milezero.mobile.model.driveritinerary.db.DriverItinerary;
import com.milezero.mobile.model.driveritinerary.db.Task;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.PUT;
import retrofit.http.Path;

/**
 * Created by suraj on 9/12/15.
 */
public interface ApiEndpointInterface {

    //methods will be used outside this package_big, so kept public

    @GET("/v2/driver/{driverExternalId}/itinerary")
//    public void getDriverItinerary(@Path("driverExternalId") String driverExternalId, Callback<String> response);
    Call<DriverItinerary> getDriverItinerary(@Path("driverExternalId") String driverExternalId);

//    @GET("/v2/5677fee70f00005116500802")
//    @GET("/v2/56786d6d0f00007f2c500867")
//    @GET("/v2/567870b40f0000e42c50086a")
//        @GET("/v2/567880e00f00008630500875")
//        @GET("/v2/568204461200008e3593a2b4")
//        @GET("/v2/568234bf120000294093a2d1")
//        @GET("/v2/568e1fd90f00007939d18303")
        @GET("/v2/56c5bf740f00005228a204eb")
    Call<DriverItinerary> getDriverItineraryDummy();


    @Headers("X-MZ-AUTH-API-KEY: eaze1234")
    @PUT("/v2/driver/{driverExternalId}/taskProgress")
    Call<Void> updateTask(@Body Task updateTaskStatus,  @Path("driverExternalId") String driverExternalId);
}
