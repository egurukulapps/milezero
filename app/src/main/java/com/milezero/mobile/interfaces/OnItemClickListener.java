package com.milezero.mobile.interfaces;

import android.view.View;

/**
 * Created by nishant on 11/1/16.
 */
public interface OnItemClickListener {
    void onItemClick(View view, int position);
}
