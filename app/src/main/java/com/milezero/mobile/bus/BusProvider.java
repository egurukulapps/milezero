package com.milezero.mobile.bus;

import com.squareup.otto.Bus;

/**
 * Created by Suraj Dubey on 13/12/15.
 */
public class BusProvider {
    private static Bus BUS = new Bus();

    public static Bus getInstance()
    {
        return BUS;
    }

    private BusProvider()
    {
        //No instance
    }
}
