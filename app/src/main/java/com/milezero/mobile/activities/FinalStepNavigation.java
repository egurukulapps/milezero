package com.milezero.mobile.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.AvoidType;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.StreetViewPanoramaLocation;
import com.milezero.mobile.R;
import com.milezero.mobile.db.realm.RealmSingleton;
import com.milezero.mobile.model.driveritinerary.db.Address;
import com.milezero.mobile.model.driveritinerary.db.DriverItinerary;
import com.milezero.mobile.model.driveritinerary.db.ExecutionWindow;
import com.milezero.mobile.model.driveritinerary.db.Stop;
import com.milezero.mobile.model.driveritinerary.db.Task;
import com.milezero.mobile.utils.Config;
import com.milezero.mobile.utils.L;
import com.milezero.mobile.utils.ShowToast;
import com.milezero.mobile.utils.Validation;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by nishant on 31/12/15.
 */
public class FinalStepNavigation extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, OnStreetViewPanoramaReadyCallback {

    Marker mPositionMarker;
    GoogleMap mMap;

    Context mContext = this;
    double latitude;
    double longitude;

    int currentTaskIndex;
    int currentStopIndex;

    Stop currentStop;
    Task currentTask;

    TextView tvAddressLine1;



    TextView tvContactName;

    private LatLng destination;
    GoogleApiClient mGoogleApiClient;

    Realm realm;
    DriverItinerary itinerary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_step_navigation);
        setUIComponent();
        getIntentData();
        setIntentData();
        setTaskStopData();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        StreetViewPanoramaFragment streetViewPanoramaFragment =
                (StreetViewPanoramaFragment) getFragmentManager()
                        .findFragmentById(R.id.streetview);
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(this);


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, StreetViewActivity.class);
                intent.putExtra("lat", destination.latitude);
                intent.putExtra("lng", destination.longitude);
                intent.putExtra("currentStopIndex", currentStopIndex);

                startActivity(intent);

            }
        });


    }


    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {

        L.e("lat = " + latitude + "  lng = " + longitude);
        streetViewPanorama.setPosition(new LatLng(latitude, longitude));

        streetViewPanorama.setOnStreetViewPanoramaChangeListener(new StreetViewPanorama.OnStreetViewPanoramaChangeListener() {
            @Override
            public void onStreetViewPanoramaChange(StreetViewPanoramaLocation streetViewPanoramaLocation) {
                if (streetViewPanoramaLocation != null && streetViewPanoramaLocation.links != null) {
                } else {
                    ShowToast.showToast(mContext, mContext.getResources().getString(R.string.no_street_view));
                }
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    private void getRoute(final LatLng origin, final LatLng destination) {
        GoogleDirection.withServerKey(Config.GOOGLE_API_KEY_DIRECTION)
                .from(origin)
                .to(destination)
                .avoid(AvoidType.FERRIES)
                .avoid(AvoidType.HIGHWAYS)
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction) {
                        L.e(direction.getStatus());

                        if (direction.isOK()) {

                            L.e("ok");

                            mMap.addMarker(new MarkerOptions().position(destination));

                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(origin, Config.MAPS_ZOOM_IN_LEVEL_EXTREME));

                            ArrayList<LatLng> directionPositionList = direction.getRouteList().get(0).getLegList().get(0).getDirectionPoint();
                            mMap.addPolyline(DirectionConverter.createPolyline(mContext, directionPositionList, 5, Color.GRAY));


                        } else {
                            // Do something
                        }
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {
                        // Do something
                        L.e(t.getMessage());
                    }
                });
    }


    private void getIntentData() {
        Intent intent = getIntent();
        if (intent != null) {
            currentTaskIndex = intent.getIntExtra("currentTaskIndex", 0);
            currentStopIndex = intent.getIntExtra("currentStopIndex", 0);

        }
    }

    private void setIntentData() {
        realm = RealmSingleton.getInstance(mContext);
        RealmQuery<DriverItinerary> query = realm.where(DriverItinerary.class);

        RealmResults<DriverItinerary> itineraries = query.findAll();
        itinerary = itineraries.get(0);
        currentStop = itinerary.getStops().get(currentStopIndex);


        //set map data
        longitude = currentStop.getGeocode().getLongitude();
        latitude = currentStop.getGeocode().getLatitude();
        destination = new LatLng(currentStop.getGeocode().getLatitude(), currentStop.getGeocode().getLongitude());

    }


    @Override
    public void onLocationChanged(Location location) {

        L.e("onLocationChanged");
        if (location == null)
            return;

        if (mPositionMarker == null) {

            mPositionMarker = mMap.addMarker(new MarkerOptions()
                    .flat(true)
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.sort_up))
                    .anchor(0.5f, 0.5f)
                    .position(
                            new LatLng(location.getLatitude(), location
                                    .getLongitude())));
        }

        LatLng latLng = new LatLng(location
                .getLatitude(), location.getLongitude());
//        animateMarker(mPositionMarker, latLng, false, mMap); // Helper method for smooth
        // animation

        animateMarker(mPositionMarker, location);

        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnected(Bundle bundle) {
        createLocationRequest();

        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        getRoute(new LatLng(location.getLatitude(), location.getLongitude()), destination);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        LatLng locationLatLng = new LatLng(latitude, longitude);
//        googleMap.addMarker(new MarkerOptions().position(locationLatLng).title("Title to keep"));

//        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(locationLatLng, Config.MAPS_ZOOM_IN_LEVEL));

    }


    private void createLocationRequest() {
        L.e("Inside createLocationRequest");
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);


    }


    public void animateMarker(final Marker marker, final Location location) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final LatLng startLatLng = marker.getPosition();
        final double startRotation = marker.getRotation();
        final long duration = 500;

        L.e("animate");

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);

                double lng = t * location.getLongitude() + (1 - t)
                        * startLatLng.longitude;
                double lat = t * location.getLatitude() + (1 - t)
                        * startLatLng.latitude;

                float rotation = (float) (t * location.getBearing() + (1 - t)
                        * startRotation);

                marker.setPosition(new LatLng(lat, lng));
                marker.setRotation(rotation);

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    private void setUIComponent() {

        tvAddressLine1 = (TextView) findViewById(R.id.tvAddressLine1);

        tvContactName = (TextView) findViewById(R.id.tvContactName);


    }


    private void setTaskStopData() {

        currentStop = itinerary.getStops().get(currentStopIndex);

        currentTask = currentStop.getTasks().get(0);


        Address address = currentStop.getAddress();

        String name = currentStop.getContact().getName();
        String street = address.getStreet();
        String unit = address.getUnit();
        String city = address.getCity();
        String state = address.getState();
        String postalCode = address.getPostalCode();
        final String contactNumber = currentStop.getContact().getPhone();

        String addressString = "";
        if (Validation.isValidString(street)) {

            addressString+=street;
        }
        if (Validation.isValidString(unit)) {

            addressString+=" "+unit;
        }

        addressString+=" "+postalCode;
        tvAddressLine1.setText(addressString);


        //set map data
        longitude = currentStop.getGeocode().getLongitude();
        latitude = currentStop.getGeocode().getLatitude();


        if (Validation.isValidString(name)) {
            tvContactName.setText(name);
        }


    }
}
