package com.milezero.mobile.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.milezero.mobile.R;
import com.milezero.mobile.adapter.recyclerViewAdapter.MessagesAdapter;
import com.milezero.mobile.model.messages.Messages;
import com.milezero.mobile.utils.ShowToast;

import java.util.ArrayList;

/**
 * Created by nishant on 16/1/16.
 */
public class MessagesActivity extends AppCompatActivity {
    ArrayList<Messages> messagesArrayList;

    MessagesAdapter messagesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_messages_main);

        ListView listView = (ListView) findViewById(R.id.messages);
        final EditText editText = (EditText) findViewById(R.id.inputMessage);
        ImageView btnSend = (ImageView) findViewById(R.id.btnSend);

        final String contactNumber = getIntent().getStringExtra("contactNumber");

        messagesArrayList = setUpDummyList();

        messagesAdapter = new MessagesAdapter(this, messagesArrayList);

        listView.setAdapter(messagesAdapter);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txt = editText.getText().toString();

                if (txt.isEmpty()) {
                    ShowToast.showToast(MessagesActivity.this, "Enter text");
                } else {
                    editText.setText("");
                    addMessage(txt);
                }
            }
        });

        findViewById(R.id.imgCall).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callContact(contactNumber);
            }
        });

        findViewById(R.id.finish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void addMessage(String txt) {

        Messages messages = new Messages();

        messages.setMessage(txt);
        messages.setIsSelf(true);

        messagesArrayList.add(messages);

        messagesAdapter.notifyDataSetChanged();


    }


    private void callContact(String contactNumber) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + contactNumber));
        startActivity(callIntent);
    }


    private ArrayList<Messages> setUpDummyList() {

        ArrayList<Messages> messagesArrayList = new ArrayList<>();

        Messages messages1 = new Messages();
        Messages messages2 = new Messages();
        Messages messages3 = new Messages();

        messages1.setIsSelf(false);
        messages1.setMessage("Would you leave the package_big behind the gate?");

        messages2.setIsSelf(true);
        messages2.setMessage("Please share any gate code for entry");

        messages3.setIsSelf(false);
        messages3.setMessage("#5346");

        messagesArrayList.add(messages1);
        messagesArrayList.add(messages2);
        messagesArrayList.add(messages3);

        return messagesArrayList;


    }


}
