package com.milezero.mobile.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.AvoidType;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.milezero.mobile.R;
import com.milezero.mobile.db.realm.RealmSingleton;
import com.milezero.mobile.model.driveritinerary.db.Address;
import com.milezero.mobile.model.driveritinerary.db.DriverItinerary;
import com.milezero.mobile.model.driveritinerary.db.ExecutionWindow;
import com.milezero.mobile.model.driveritinerary.db.Stop;
import com.milezero.mobile.model.driveritinerary.db.Task;
import com.milezero.mobile.utils.Config;
import com.milezero.mobile.utils.L;
import com.milezero.mobile.utils.Validation;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by nishant on 17/12/15.
 */
public class NavigationActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    Marker mPositionMarker;
    GoogleMap mMap;

    Context mContext = this;
    double latitude;
    double longitude;

    int currentTaskIndex;
    int currentStopIndex;

    Stop currentStop;
    Task currentTask;

    TextView tvExecutionWindow;
    TextView tvAddressLine1;
    TextView tvAddressLine2;
    TextView tvAddressLine3;

    TextView tvPayloadMessage;
    TextView tvPayloadHint;

    TextView tvNotes;
    TextView tvMessage;
    TextView tvContactName;

    private LatLng destination;
    GoogleApiClient mGoogleApiClient;

    Realm realm;
    DriverItinerary itinerary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        setUIComponent();
        getIntentData();
        setIntentData();
        setTaskStopData();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(NavigationActivity.this, FinalStepNavigation.class);
                intent.putExtra("lat", destination.latitude);
                intent.putExtra("lng", destination.longitude);
                intent.putExtra("currentStopIndex", currentStopIndex);

                startActivity(intent);

            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();

        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    private void getRoute(final LatLng origin, final LatLng destination) {
        GoogleDirection.withServerKey(Config.GOOGLE_API_KEY_DIRECTION)
                .from(origin)
                .to(destination)
                .avoid(AvoidType.FERRIES)
                .avoid(AvoidType.HIGHWAYS)
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction) {
                        L.e(direction.getStatus());

                        if (direction.isOK()) {

                            L.e("ok");

                            mMap.addMarker(new MarkerOptions().position(destination));

                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(origin, Config.MAPS_ZOOM_IN_LEVEL));

                            ArrayList<LatLng> directionPositionList = direction.getRouteList().get(0).getLegList().get(0).getDirectionPoint();
                            mMap.addPolyline(DirectionConverter.createPolyline(mContext, directionPositionList, 5, Color.GRAY));


                        } else {
                            // Do something
                        }
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {
                        // Do something
                        L.e(t.getMessage());
                    }
                });
    }


    private void getIntentData() {
        Intent intent = getIntent();
        if (intent != null) {
            currentTaskIndex = intent.getIntExtra("currentTaskIndex", 0);
            currentStopIndex = intent.getIntExtra("currentStopIndex", 0);

        }
    }

    private void setIntentData() {
        realm = RealmSingleton.getInstance(mContext);
        RealmQuery<DriverItinerary> query = realm.where(DriverItinerary.class);

        RealmResults<DriverItinerary> itineraries = query.findAll();
        itinerary = itineraries.get(0);
        currentStop = itinerary.getStops().get(currentStopIndex);


        //set map data
        longitude = currentStop.getGeocode().getLongitude();
        latitude = currentStop.getGeocode().getLatitude();
        destination = new LatLng(currentStop.getGeocode().getLatitude(), currentStop.getGeocode().getLongitude());

    }


    @Override
    public void onLocationChanged(Location location) {

        L.e("onLocationChanged");
        if (location == null)
            return;

        if (mPositionMarker == null) {

            mPositionMarker = mMap.addMarker(new MarkerOptions()
                    .flat(true)
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.sort_up))
                    .anchor(0.5f, 0.5f)
                    .position(
                            new LatLng(location.getLatitude(), location
                                    .getLongitude())));
        }

        LatLng latLng = new LatLng(location
                .getLatitude(), location.getLongitude());
//        animateMarker(mPositionMarker, latLng, false, mMap); // Helper method for smooth
        // animation

        animateMarker(mPositionMarker, location);

        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnected(Bundle bundle) {
        createLocationRequest();

        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        getRoute(new LatLng(location.getLatitude(), location.getLongitude()), destination);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        LatLng locationLatLng = new LatLng(latitude, longitude);
//        googleMap.addMarker(new MarkerOptions().position(locationLatLng).title("Title to keep"));

//        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(locationLatLng, Config.MAPS_ZOOM_IN_LEVEL));

    }


    private void createLocationRequest() {
        L.e("Inside createLocationRequest");
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);


    }


    public void animateMarker(final Marker marker, final Location location) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final LatLng startLatLng = marker.getPosition();
        final double startRotation = marker.getRotation();
        final long duration = 500;

        L.e("animate");

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);

                double lng = t * location.getLongitude() + (1 - t)
                        * startLatLng.longitude;
                double lat = t * location.getLatitude() + (1 - t)
                        * startLatLng.latitude;

                float rotation = (float) (t * location.getBearing() + (1 - t)
                        * startRotation);

                marker.setPosition(new LatLng(lat, lng));
                marker.setRotation(rotation);

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hideMarker, GoogleMap googleMap) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = googleMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }

    private void setUIComponent() {
        tvExecutionWindow = (TextView) findViewById(R.id.tvExecutionWindow);
        tvMessage = (TextView) findViewById(R.id.tvMessage);
        tvNotes = (TextView) findViewById(R.id.tvNotes);
        tvPayloadHint = (TextView) findViewById(R.id.tvPayloadHint);
        tvPayloadMessage = (TextView) findViewById(R.id.tvPayloadMessage);

        tvAddressLine1 = (TextView) findViewById(R.id.tvAddressLine1);
        tvAddressLine2 = (TextView) findViewById(R.id.tvAddressLine2);
        tvAddressLine3 = (TextView) findViewById(R.id.tvAddressLine3);

        tvContactName = (TextView) findViewById(R.id.tvContactName);


    }


    private void setTaskStopData() {

        currentStop = itinerary.getStops().get(currentStopIndex);

        currentTask = currentStop.getTasks().get(0);
        ExecutionWindow window = currentTask.getExecutionWindow();

        if (window != null) {
            if (window.getStart() != null) {
//                tvExecutionWindow.setText(window.getStart() + " " + window.getEnd());
            } else {
                tvExecutionWindow.setText("No Execution Windows Available");
            }
        }

        Address address = currentStop.getAddress();

        String name = currentStop.getContact().getName();
        String street = address.getStreet();
        String unit = address.getUnit();
        String city = address.getCity();
        String state = address.getState();
        String postalCode = address.getPostalCode();
        final String contactNumber = currentStop.getContact().getPhone();

        if (Validation.isValidString(street)) {
            tvAddressLine1.setText(street);
        }
        if (Validation.isValidString(unit)) {
            tvAddressLine2.setText(unit);
        }

        tvAddressLine3.setText(city + "," + state + "," + postalCode);

        //set map data
        longitude = currentStop.getGeocode().getLongitude();
        latitude = currentStop.getGeocode().getLatitude();

        if (currentTask.getHasPayload()) {
            int payloadSize = currentTask.getPayload().size();
            tvPayloadMessage.setText("Deliver " + payloadSize + " packages");
        }

        if (Validation.isValidString(name)) {
            tvContactName.setText(name);
        }


    }
}
