package com.milezero.mobile.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;

import com.milezero.mobile.R;
import com.milezero.mobile.fragments.ArrivalConfirmationFragment;
import com.milezero.mobile.fragments.CollectPaymentFragment;
import com.milezero.mobile.fragments.DisburseChangesFragment;
import com.milezero.mobile.fragments.GetPackagesFragment;
import com.milezero.mobile.fragments.PackageUndeliverable;
import com.milezero.mobile.fragments.PackageViewFragment;
import com.milezero.mobile.fragments.PaymentCollectedFragment;
import com.milezero.mobile.utils.ManageFragments;

/**
 * Created by nishant on 10/1/16.
 */
public class ContainerLayout extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_main);

        manageFragments("get_packages_fragment", null);


    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    public void manageFragments(String newState, Bundle bundle) {
        ManageFragments manageFragments = new ManageFragments();
        switch (newState) {

            case "arrival_confirm":

                manageFragments.showFragmentNoEnter(this, R.id.mainLayout, false, true, new ArrivalConfirmationFragment(), "arrival_confirm", null);


                break;

            case "get_packages_fragment":

                manageFragments.showFragmentNoEnter(this, R.id.mainLayout, false, true, new GetPackagesFragment(), "get_packages_fragment", null);


                break;

            case "package_view_fragment":

                manageFragments.showFragmentNoEnter(this, R.id.mainLayout, false, true, new PackageViewFragment(), "package_view_fragment", bundle);


                break;

            case "PackageUndeliverable":

                manageFragments.showFragmentNoEnter(this, R.id.mainLayout, false, true, new PackageUndeliverable(), "PackageUndeliverable", bundle);


                break;

            case "collect":

                manageFragments.showFragmentNoEnter(this, R.id.mainLayout, false, true, new CollectPaymentFragment(), "collect", bundle);


                break;

            case "collected":

                manageFragments.showFragment(this, R.id.mainLayout, false, true, new PaymentCollectedFragment(), "collected", bundle);


                break;
            case "change":

                manageFragments.showFragment(this, R.id.mainLayout, false, true, new DisburseChangesFragment(), "change", bundle);


                break;

            default:
                break;

        }
    }
}
