package com.milezero.mobile.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.StreetViewPanoramaLocation;
import com.milezero.mobile.R;
import com.milezero.mobile.db.realm.RealmSingleton;
import com.milezero.mobile.model.driveritinerary.db.Address;
import com.milezero.mobile.model.driveritinerary.db.DriverItinerary;
import com.milezero.mobile.model.driveritinerary.db.ExecutionWindow;
import com.milezero.mobile.model.driveritinerary.db.Stop;
import com.milezero.mobile.model.driveritinerary.db.Task;
import com.milezero.mobile.utils.L;
import com.milezero.mobile.utils.ShowToast;
import com.milezero.mobile.utils.Validation;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by nishant on 30/12/15.
 */
public class StreetViewActivity extends FragmentActivity implements OnStreetViewPanoramaReadyCallback {


    double lat, lng;

    Context context = StreetViewActivity.this;

    int currentStopIndex;

    Stop currentStop;
    Task currentTask;

    TextView tvExecutionWindow;
    TextView tvAddressLine1;
    TextView tvAddressLine2;
    TextView tvAddressLine3;

    TextView tvPayloadMessage;
    TextView tvPayloadHint;

    TextView tvNotes;
    TextView tvMessage;
    TextView tvContactName;

    Realm realm;
    DriverItinerary itinerary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_street_view);

        setUIComponent();
        setIntentData();
        getIntentData();
        setTaskStopData();

        StreetViewPanoramaFragment streetViewPanoramaFragment =
                (StreetViewPanoramaFragment) getFragmentManager()
                        .findFragmentById(R.id.streetview);
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(this);

        Intent intent = getIntent();

        lat = intent.getDoubleExtra("lat", 0.0);
        lng = intent.getDoubleExtra("lng", 0.0);

        findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(context, ContainerLayout.class);
                intent1.putExtra("currentStopIndex", currentStopIndex);
                startActivity(intent1);

            }
        });




    }

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {

        L.e("lat = " + lat + "  lng = " + lng);
        streetViewPanorama.setPosition(new LatLng(lat, lng));

        streetViewPanorama.setOnStreetViewPanoramaChangeListener(new StreetViewPanorama.OnStreetViewPanoramaChangeListener() {
            @Override
            public void onStreetViewPanoramaChange(StreetViewPanoramaLocation streetViewPanoramaLocation) {
                if (streetViewPanoramaLocation != null && streetViewPanoramaLocation.links != null) {
                } else {
                    ShowToast.showToast(context, context.getResources().getString(R.string.no_street_view));
                }
            }
        });

    }

    private void getIntentData() {
        Intent intent = getIntent();
        if (intent != null) {

            currentStopIndex = intent.getIntExtra("currentStopIndex", 0);

        }
    }

    private void setIntentData() {
        realm = RealmSingleton.getInstance(context);
        RealmQuery<DriverItinerary> query = realm.where(DriverItinerary.class);

        RealmResults<DriverItinerary> itineraries = query.findAll();
        itinerary = itineraries.get(0);


    }


    private void setUIComponent() {
        tvExecutionWindow = (TextView) findViewById(R.id.tvExecutionWindow);
        tvMessage = (TextView) findViewById(R.id.tvMessage);
        tvNotes = (TextView) findViewById(R.id.tvNotes);
        tvPayloadHint = (TextView) findViewById(R.id.tvPayloadHint);
        tvPayloadMessage = (TextView) findViewById(R.id.tvPayloadMessage);

        tvAddressLine1 = (TextView) findViewById(R.id.tvAddressLine1);
        tvAddressLine2 = (TextView) findViewById(R.id.tvAddressLine2);
        tvAddressLine3 = (TextView) findViewById(R.id.tvAddressLine3);

        tvContactName = (TextView) findViewById(R.id.tvContactName);




    }


    private void setTaskStopData() {

        currentStop = itinerary.getStops().get(currentStopIndex);

        currentTask = currentStop.getTasks().get(0);
        ExecutionWindow window = currentTask.getExecutionWindow();

        if (window != null) {
            if (window.getStart() != null) {
//                tvExecutionWindow.setText(window.getStart() + " " + window.getEnd());
            } else {
                tvExecutionWindow.setText("No Execution Windows Available");
            }
        }

        Address address = currentStop.getAddress();

        String name = currentStop.getContact().getName();
        String street = address.getStreet();
        String unit = address.getUnit();
        String city = address.getCity();
        String state = address.getState();
        String postalCode = address.getPostalCode();
        final String contactNumber = currentStop.getContact().getPhone();

        if (Validation.isValidString(street)) {
            tvAddressLine1.setText(street);
        }
        if (Validation.isValidString(unit)) {
            tvAddressLine2.setText(unit);
        }

        tvAddressLine3.setText(city + "," + state + "," + postalCode);

        if (currentTask.getHasPayload()) {
            int payloadSize = currentTask.getPayload().size();
            tvPayloadMessage.setText("Deliver " + payloadSize + " packages");
        }

        if (Validation.isValidString(name)) {
            tvContactName.setText(name);
        }


    }
}