package com.milezero.mobile.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.milezero.mobile.R;
import com.milezero.mobile.db.realm.RealmSingleton;
import com.milezero.mobile.listener.gestureListener.SimpleGestureFilter;
import com.milezero.mobile.model.driveritinerary.db.Address;
import com.milezero.mobile.model.driveritinerary.db.DriverItinerary;
import com.milezero.mobile.model.driveritinerary.db.ExecutionWindow;
import com.milezero.mobile.model.driveritinerary.db.Stop;
import com.milezero.mobile.model.driveritinerary.db.Task;
import com.milezero.mobile.utils.BasicUtils;
import com.milezero.mobile.utils.Config;
import com.milezero.mobile.utils.L;
import com.milezero.mobile.utils.Validation;
import com.squareup.otto.Subscribe;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;


public class RouteDetailActivity extends FragmentActivity implements OnMapReadyCallback, SimpleGestureFilter.SimpleGestureListener {

    Context mContext = this;
    double latitude;
    double longitude;

    //    int currentTaskIndex;
//    int currentStopIndex;
    int currentPosition;
    int taskSize;


    Task currentTask;
    Stop currentStop;

    //UI Components
    TextView tvExecutionWindow;
    TextView tvAddressLine1;
    TextView tvAddressLine2;
    TextView tvAddressLine3;

    TextView tvPayloadMessage;
    TextView tvPayloadHint;

    TextView tvNotes;
    TextView tvMessage, stopNumber;
    TextView tvContactName;

    ImageView imgCall;
    ImageView imgMessage;

    Realm realm;
    DriverItinerary itinerary;

    int totalStops;
    GoogleMap googleMap;

    private SimpleGestureFilter simpleGestureFilter;

//    ArrayList<TaskData> taskList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_detail_test);
        LinearLayout swipeLayout = (LinearLayout) findViewById(R.id.swipeLayout);
        simpleGestureFilter = new SimpleGestureFilter(mContext, this);
//        final GestureDetector gestureDetector = new GestureDetector(this, simpleGestureFilter);

        swipeLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                simpleGestureFilter.onTouchEvent(event);
                return true;
            }
        });

        setUIComponent();
        getIntentData();
//        setRealmData();
        setIntentData();
        setTaskStopData();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //listener for start navigation button
        Button btnStartNavigation = (Button) findViewById(R.id.btnStartNavigation);
        btnStartNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //send longitude and latitude
                Intent navigationIntent = new Intent(mContext, HereMapsNavigationActivity.class);
                navigationIntent.putExtra("latitude", latitude);
                navigationIntent.putExtra("longitude", longitude);
                startActivity(navigationIntent);
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // add marker to location
        this.googleMap = googleMap;
        LatLng locationLatLng = new LatLng(latitude, longitude);
//        googleMap.addMarker(new MarkerOptions().position(locationLatLng).title("Title to keep"));
        makeMarkers(googleMap);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(locationLatLng, Config.MAPS_ZOOM_IN_LEVEL));
    }


    private void makeMarkers(GoogleMap googleMap) {
        IconGenerator tc = new IconGenerator(this);


        if (itinerary != null) {
            RealmList<Stop> stops = itinerary.getStops();
            for (int i = 0; i < stops.size(); i++) {
                Stop stop = stops.get(i);
                if (stop.getGeocode() != null) {
                    LatLng locationLatLng = new LatLng(stop.getGeocode().getLatitude(), stop.getGeocode().getLongitude());

                    if (i == currentPosition) {
                        tc.setStyle(IconGenerator.STYLE_ORANGE);
                    } else {
                        tc.setStyle(IconGenerator.STYLE_BLUE);
                    }

                    googleMap.addMarker(new MarkerOptions()
                            .position(locationLatLng)
                            .icon(BitmapDescriptorFactory.fromBitmap(tc.makeIcon(stop.getStopNumber() + ""))));
                }


            }
        }

    }


    public BitmapDescriptor getTextMarker(String text) {

        Paint paint = new Paint();
    /* Set text size, color etc. as needed */
        paint.setTextSize(24);

        int width = (int) paint.measureText(text);
        int height = (int) paint.getTextSize();

        paint.setTextAlign(Paint.Align.CENTER);
        // Create a transparent bitmap as big as you need
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(image);
        // During development the following helps to see the full
        // drawing area:
        canvas.drawColor(Color.BLUE);
        // Start drawing into the canvas
        canvas.translate(width / 2f, height);
        canvas.drawText(text, 0, 0, paint);
        BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(image);
        return icon;
    }

    private void getIntentData() {
        Intent intent = getIntent();

        if (intent != null) {
            currentPosition = intent.getIntExtra("currentPosition", 0);

//            taskList = TaskListSingleton.taskList;
//            L.d(taskList.size()+" ");
        }
    }

//    private void setRealmData() {
//        Intent intent = getIntent();
//
//        if (intent != null) {
//            currentTaskIndex = intent.getIntExtra("currentTaskIndex", 0);
//            currentStopIndex = intent.getIntExtra("currentStopIndex", 0);
////            latitude = intent.getDoubleExtra("latitude", 0);
////            longitude = intent.getDoubleExtra("longitude", 0);
////            L.d(latitude + " " + longitude);
////
////            L.d("Task: "+intent.getStringExtra("currentStop"));
////            L.d("Stop: "+intent.getStringExtra("currentTask"));
////            currentStop = new Gson().fromJson(intent.getStringExtra("currentStop"), Stop.class);
////            currentTask = new Gson().fromJson(intent.getStringExtra("currentTask"), Task.class);
//        }
//    }

    private void setIntentData() {

        realm = RealmSingleton.getInstance(mContext);
        RealmQuery<DriverItinerary> query = realm.where(DriverItinerary.class);

        RealmResults<DriverItinerary> itineraries = query.findAll();
        itinerary = itineraries.get(0);


    }


    private void setTaskStopData() {

//        currentTask = taskList.get(currentPosition).getTask();

//        currentStop = itinerary.getStops().get(taskList.get(currentPosition).getStopId());
        totalStops = itinerary.getStops().size();
        currentStop = itinerary.getStops().get(currentPosition);

        currentTask = currentStop.getTasks().get(0);
        ExecutionWindow window = currentTask.getExecutionWindow();

//        stopNumber.setText(String.valueOf(currentStop.getStopNumber()));

        if (window != null) {
            if (window.getStart() != null) {
//                tvExecutionWindow.setText(window.getStart() + " " + window.getEnd());
            } else {
                tvExecutionWindow.setText("No Execution Windows Available");
            }
        }

        Address address = currentStop.getAddress();

        String name = currentStop.getContact().getName();
        String street = address.getStreet();
        String unit = address.getUnit();
        String city = address.getCity();
        String state = address.getState();
        String postalCode = address.getPostalCode();
        final String contactNumber = currentStop.getContact().getPhone();

        if (Validation.isValidString(street)) {
            tvAddressLine1.setText(street);
        }
        if (Validation.isValidString(unit)) {
            tvAddressLine2.setText(unit);
        }

        tvAddressLine3.setText(city + "," + state + "," + postalCode);

        //set map data
        longitude = currentStop.getGeocode().getLongitude();
        latitude = currentStop.getGeocode().getLatitude();

        if (currentTask.getHasPayload()) {
            int payloadSize = currentTask.getPayload().size();
            tvPayloadMessage.setText("Deliver " + payloadSize + " packages");
        }

        if (Validation.isValidString(name)) {
            tvContactName.setText(name);
        }

        imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callContact(contactNumber);
            }
        });


        imgMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(RouteDetailActivity.this, MessagesActivity.class);

                intent.putExtra("contactNumber", contactNumber);

                startActivity(intent);


            }
        });

        findViewById(R.id.btnNavigate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (BasicUtils.checkGps(RouteDetailActivity.this)) {
                    onNavigate();
                }

            }
        });


        if (googleMap != null) {
            LatLng locationLatLng = new LatLng(latitude, longitude);
//            googleMap.addMarker(new MarkerOptions().position(locationLatLng).title("Title to keep"));

            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(locationLatLng, Config.MAPS_ZOOM_IN_LEVEL));
        }


    }


    @Override
    protected void onStart() {
        super.onStart();
    }

//    @Override
//    public boolean dispatchTouchEvent(MotionEvent ev) {
////        this.gestureDetector.onTouchEvent(ev);
//        return super.dispatchTouchEvent(ev);
//    }

    @Override
    public void swipe(int direction) {
        L.e("current pos = " + currentPosition);
        if (direction == SimpleGestureFilter.SWIPE_LEFT) {
            if (currentPosition == 1) {
                Toast.makeText(mContext, "This is first entry", Toast.LENGTH_SHORT).show();
            } else {
//                currentPosition--;
                getPreviousStop();
                setTaskStopData();
            }
            L.d("Left Swipe");
        } else if (direction == SimpleGestureFilter.SWIPE_RIGHT) {
            if (currentPosition == totalStops - 1) {
                Toast.makeText(mContext, "This is last Entry", Toast.LENGTH_SHORT).show();
            } else {
//                currentPosition++;
                getNextStop();
                setTaskStopData();
            }
            L.d("Right Swipe");
        }
    }


    private void getPreviousStop() {

        for (int i = currentPosition - 1; i > 0; i--) {
            currentPosition--;
            Stop stop = itinerary.getStops().get(i);

            if (stop.getGeocode() != null) {
                return;
            }


        }

    }

    private void getNextStop() {

        for (int i = currentPosition + 1; i < totalStops; i++) {
            currentPosition++;
            Stop stop = itinerary.getStops().get(i);

            if (stop.getGeocode() != null) {
                return;
            }


        }

    }


    private void setUIComponent() {
        tvExecutionWindow = (TextView) findViewById(R.id.tvExecutionWindow);
        tvMessage = (TextView) findViewById(R.id.tvMessage);
        tvNotes = (TextView) findViewById(R.id.tvNotes);
        tvPayloadHint = (TextView) findViewById(R.id.tvPayloadHint);
        tvPayloadMessage = (TextView) findViewById(R.id.tvPayloadMessage);

        tvAddressLine1 = (TextView) findViewById(R.id.tvAddressLine1);
        stopNumber = (TextView) findViewById(R.id.stopNumber);
        tvAddressLine2 = (TextView) findViewById(R.id.tvAddressLine2);
        tvAddressLine3 = (TextView) findViewById(R.id.tvAddressLine3);

        tvContactName = (TextView) findViewById(R.id.tvContactName);

        imgCall = (ImageView) findViewById(R.id.imgCall);
        imgMessage = (ImageView) findViewById(R.id.imgMessage);

    }


    private void onNavigate() {
        Intent intent = new Intent(mContext, NavigationActivity.class);


        intent.putExtra("currentStopIndex", currentPosition);
        startActivity(intent);

//        String format = "geo:0,0?q=" + latitude + "," + longitude + "( Location title)";
//        Uri uri = Uri.parse(format);
//        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(intent);


    }


    private void callContact(String contactNumber) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + contactNumber));
        mContext.startActivity(callIntent);
    }

    @Subscribe
    public void handleStop(Stop stop) {
        L.d("Hello Stop");

    }

    @Override
    public void onDoubleTap() {
        L.d("Double tap");
    }
}
