package com.milezero.mobile.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.IconCategory;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.guidance.NavigationManager;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapFragment;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.RouteManager;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.milezero.mobile.R;
import com.milezero.mobile.utils.L;

import java.lang.ref.WeakReference;
import java.util.List;

public class HereMapsNavigationActivity extends AppCompatActivity {

    private Context context = this;
    private MapFragment mapFragment = null;
    private Map map = null;

    private boolean firstPositionSet = false;

    private PositioningManager mapPositioningManager = null;

    private PositioningManager.OnPositionChangedListener mapPositionChangedListener;

    private String toastMessage = "";

    private MapRoute currentRoute;

    private boolean startNavigation;

    private double destinationLatitude;
    private double destinationLongitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_here_maps_navigation);

//        setToolbar();
        setIntentData();

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment);

        mapFragment.init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(Error error) {
                if(error == Error.NONE)
                {

                    setMapPositioningManager();
                    initializePositionListener();

                    map = mapFragment.getMap();

                    map.setProjectionMode(Map.Projection.GLOBE);
                    map.setExtrudedBuildingsVisible(true);
                    map.setLandmarksVisible(true);
                    map.setCartoMarkersVisible(IconCategory.ALL, true);
                    map.setSafetySpotsVisible(true);
                    map.setMapScheme(Map.Scheme.NORMAL_DAY);

                    map.setTrafficInfoVisible(false);

                    mapPositioningManager.addListener(new WeakReference<PositioningManager.OnPositionChangedListener>(mapPositionChangedListener));

                    map.getPositionIndicator().setVisible(true);
                    map.getPositionIndicator().setAccuracyIndicatorVisible(true);

                    //use GPS, network and WiFi
                    mapPositioningManager.start(PositioningManager.LocationMethod.GPS_NETWORK);

                    GeoPosition currentGeoPosition = mapPositioningManager.getLastKnownPosition();

                    if(currentGeoPosition!=null && currentGeoPosition.isValid())
                    {
                        toastMessage = "Current Position "+currentGeoPosition.getCoordinate().getLatitude()+" "+currentGeoPosition.getCoordinate().getLongitude();
                        map.setCenter(currentGeoPosition.getCoordinate(), Map.Animation.NONE);

                    }
                    else {
                        toastMessage = "No location found";
                    }

//                    Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT).show();

                    L.d("Init: " + toastMessage );
                    NavigationManager.getInstance().setMap(map);

                    startRouting();

                }
                else
                {
                    Toast.makeText(context, "Map initialization error "+error, Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    private void setToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar()!=null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void setIntentData() {
        Intent intent = getIntent();
        if(intent!=null)
        {
            destinationLatitude = intent.getDoubleExtra("latitude", 0);
            destinationLongitude = intent.getDoubleExtra("longitude", 0);
        }
    }


    private void initializePositionListener()
    {
        mapPositionChangedListener = new PositioningManager.OnPositionChangedListener() {
            @Override
            public void onPositionUpdated(PositioningManager.LocationMethod locationMethod, GeoPosition geoPosition, boolean b) {
                if(!geoPosition.isValid())
                {
                    t("Not valid position");
                    return;
                }

                map.setCenter(geoPosition.getCoordinate(), Map.Animation.BOW);
                L.d("Position has changed");
//                t("Position changed");
            }

            @Override
            public void onPositionFixChanged(PositioningManager.LocationMethod locationMethod, PositioningManager.LocationStatus locationStatus) {
                L.d( "Position fix changed : " + locationStatus.name() + " / "+locationMethod.name());

                // only allow guidance, when we have a GPS fix
                if(locationStatus == PositioningManager.LocationStatus.AVAILABLE && locationMethod == PositioningManager.LocationMethod.GPS)
                {
                    t("Navigation to be started now");
                }
            }
        };
    }

    private void startRouting()
    {
        //if currently guidance session is running, stop it
        if(NavigationManager.getInstance().getRunningState() == NavigationManager.NavigationState.RUNNING)
        {
            L.d("Stpo running guidance...");
            NavigationManager.getInstance().stop();
            return;
        }

        L.d("Calculating new route");

        //calculate the route

        RouteOptions routeOptions = new RouteOptions();
        routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
        routeOptions.setRouteType(RouteOptions.Type.ECONOMIC);

        RoutePlan routePlan = new RoutePlan();
        routePlan.setRouteOptions(routeOptions);

        GeoCoordinate lastKnownPosition = mapPositioningManager.getLastKnownPosition().getCoordinate();

        //start route point
        routePlan.addWaypoint(lastKnownPosition);

        GeoCoordinate endCoordinate = new GeoCoordinate(destinationLatitude, destinationLongitude);
        //end point for route
        routePlan.addWaypoint(endCoordinate);

        RouteManager manager = new RouteManager();

        manager.calculateRoute(routePlan, new RouteManager.Listener() {
            @Override
            public void onProgress(int i) {

            }

            @Override
            public void onCalculateRouteFinished(RouteManager.Error error, List<RouteResult> list) {
                if(error!= RouteManager.Error.NONE)
                {
                    L.d("Route cannot be calculated");
                    return;

                }

                t("Route calculated");

                if(list != null && list.size()>0)
                {
                    if(currentRoute!=null)
                    {
                        map.removeMapObject(currentRoute);
                    }

                    currentRoute = new MapRoute(list.get(0).getRoute());
                    map.addMapObject(currentRoute);

                    startGuidance();
                }
            }
        });
    }

    private void startGuidance() {
        t("Starting guidance");

        map.setMapScheme(Map.Scheme.CARNAV_DAY);
        map.setTilt(45);
        map.setZoomLevel(18);

        // set guidance view to position with road ahead, tilt and zoomlevel was setup before manually
        // choose other update modes for different position and zoom behavior
        NavigationManager.getInstance().setMapUpdateMode(NavigationManager.MapUpdateMode.POSITION_ANIMATION);

        // get new guidance instructions
        NavigationManager.getInstance().addNewInstructionEventListener(new WeakReference<>(instructionHandler));

        // start simulation with speed of 10 m/s
//        NavigationManager.Error e = NavigationManager.getInstance().simulate(currentRoute.getRoute(), 10);

        // start real guidance
        NavigationManager.Error e = NavigationManager.getInstance().startNavigation(currentRoute.getRoute());

        t( "Guidance start result : " + e.name());


    }

    // listen for new instruction events
    private NavigationManager.NewInstructionEventListener instructionHandler = new NavigationManager.NewInstructionEventListener()
    {
        @Override
        public void onNewInstructionEvent() {
            Maneuver m = NavigationManager.getInstance().getNextManeuver();
            t( "New instruction : in "+NavigationManager.getInstance().getNextManeuverDistance()+" m do "+m.getTurn().name()+" / "+m.getAction().name() + " on "+m.getNextRoadName() + " (from "+m.getRoadName()+")" );

            // ...
            // do something with this information, e.g. show it to the user

            super.onNewInstructionEvent();
        }
    };

    public void setMapPositioningManager() {
        mapPositioningManager = PositioningManager.getInstance();
    }

    private void t(String toastMessage)
    {
        Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT).show();
    }
}
