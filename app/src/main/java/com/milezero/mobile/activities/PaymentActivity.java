package com.milezero.mobile.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.milezero.mobile.R;
import com.milezero.mobile.fragments.CollectPaymentFragment;
import com.milezero.mobile.fragments.DisburseChangesFragment;
import com.milezero.mobile.fragments.PaymentCollectedFragment;
import com.milezero.mobile.utils.ManageFragments;

/**
 * Created by nishant on 13/1/16.
 */
public class PaymentActivity extends AppCompatActivity {
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_payment);

        toolbar = (Toolbar) findViewById(R.id.toolbar);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });


        manageFragments("collect", null);


    }

    public void setTitle(String title) {

        toolbar.setTitle(title);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    public void manageFragments(String newState, Bundle bundle) {
        ManageFragments manageFragments = new ManageFragments();
        switch (newState) {

            case "collect":

                manageFragments.showFragmentNoEnter(this, R.id.mainLayout, false, true, new CollectPaymentFragment(), "collect", bundle);


                break;

            case "collected":

                manageFragments.showFragment(this, R.id.mainLayout, false, true, new PaymentCollectedFragment(), "collected", bundle);


                break;
            case "change":

                manageFragments.showFragment(this, R.id.mainLayout, false, true, new DisburseChangesFragment(), "change", bundle);


                break;

            default:
                break;

        }
    }
}
