package com.milezero.mobile.activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.milezero.mobile.R;
import com.milezero.mobile.utils.L;

/* This is launcher class
 */
public class SplashActivity extends AppCompatActivity {

    Context mContext = this;

    String accountName = "dummyAccount";
    String packageName = "com.milezero.mobile";

    String password = null;
    Bundle userData = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

//        setSyncAccount();

        //launch RouteListActivity
        Intent intent = new Intent(mContext, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void setSyncAccount() {
        Account newAccount = new Account(accountName, packageName);
        AccountManager accountManager = (AccountManager) mContext.getSystemService(ACCOUNT_SERVICE);

        accountManager.addAccountExplicitly(newAccount, password,  userData);

        ContentResolver.setSyncAutomatically(newAccount, getResources().getString(R.string.content_authority), true);
        ContentResolver.requestSync(newAccount, getResources().getString(R.string.content_authority), Bundle.EMPTY);
        L.d("sync Started");
    }
}
