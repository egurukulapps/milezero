package com.milezero.mobile.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.milezero.mobile.R;
import com.milezero.mobile.adapter.recyclerViewAdapter.PayloadAdapter;
import com.milezero.mobile.db.realm.RealmSingleton;
import com.milezero.mobile.interfaces.OnItemClickListener;
import com.milezero.mobile.model.driveritinerary.db.Address;
import com.milezero.mobile.model.driveritinerary.db.DriverItinerary;
import com.milezero.mobile.model.driveritinerary.db.Payload;
import com.milezero.mobile.model.driveritinerary.db.Stop;
import com.milezero.mobile.model.driveritinerary.db.Task;
import com.milezero.mobile.utils.L;
import com.milezero.mobile.utils.Validation;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by nishant on 7/1/16.
 */
public class GetPackagesActivity extends FragmentActivity {


    DriverItinerary itinerary;

    Realm realm;

    int currentStopIndex;
    Context mContext = this;

    TextView tvAddressLine1, itemCount;
    RealmList<Payload> payloads;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_get_packages);


        tvAddressLine1 = (TextView) findViewById(R.id.tvAddressLine1);
        itemCount = (TextView) findViewById(R.id.itemCount);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);

        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        getIntentData();
        setIntentData();
        payloads = getPayLoad();
        setTaskStopData();



        if (payloads != null) {
            PayloadAdapter payloadAdapter = new PayloadAdapter(payloads, mContext, new OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {

                }
            });

            recyclerView.setAdapter(payloadAdapter);
        }


    }


    private void getIntentData() {
        Intent intent = getIntent();

        if (intent != null) {
            currentStopIndex = intent.getIntExtra("currentStopIndex", 0);

        }
    }


    private void setIntentData() {


        realm = RealmSingleton.getInstance(mContext);
        RealmQuery<DriverItinerary> query = realm.where(DriverItinerary.class);

        RealmResults<DriverItinerary> itineraries = query.findAll();
        itinerary = itineraries.get(0);


    }


    private RealmList<Payload> getPayLoad() {

        Task task = itinerary.getStops().get(currentStopIndex).getTasks().get(1);

        if (task.getHasPayload()) {
            RealmList<Payload> payloadRealmList = task.getPayload();

            L.e(payloadRealmList.size() + "   " + payloadRealmList.get(0).getItem_dimension());
            return payloadRealmList;
        }

        L.e("pay load null");
        return null;


    }


    private void setTaskStopData() {

        Stop currentStop = itinerary.getStops().get(currentStopIndex);


        Address address = currentStop.getAddress();

        String street = address.getStreet();
        String unit = address.getUnit();

        String postalCode = address.getPostalCode();


        String addressString = "";
        if (Validation.isValidString(street)) {

            addressString += street;
        }
        if (Validation.isValidString(unit)) {

            addressString += " " + unit;
        }

        addressString += " " + postalCode;
        tvAddressLine1.setText(addressString);

        if(payloads!=null){
            itemCount.setText(payloads.size()+" Parcels");
        }



    }

}
