package com.milezero.mobile.listener.gestureListener;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Created by Suraj Dubey on 18/12/15.
 */
public class SimpleGestureFilter extends GestureDetector.SimpleOnGestureListener {
    public static final int SWIPE_UP = 1;
    public static final int SWIPE_DOWN = 2;
    public static final int SWIPE_LEFT = 3;
    public static final int SWIPE_RIGHT = 4;

    public static final int MODE_TRANSPARENT = 1;
    public static final int MODE_SOLID = 1;
    public static final int MODE_DYNAMIC = 1;

    public static final int ACTION_FAKE = -1;

    private int swipe_Min_Distance = 100;
    private int swipe_Max_Distance = 350;
    private int swipe_Min_Velocity = 100;

    private int mode = MODE_DYNAMIC;
    private boolean running = true;
    private boolean tapIndicator = false;

    private Context context;
    private GestureDetector detector;
    private SimpleGestureListener listener;

    public SimpleGestureFilter(Context context, SimpleGestureListener sgl)
    {
        this.context = context;
        this.detector = new GestureDetector(context, this);
        this.listener = sgl;
    }

    public void onTouchEvent(MotionEvent event)
    {
        if(!this.running) {
            return;
        }

        boolean result = detector.onTouchEvent(event);
        if(mode == MODE_SOLID)
            event.setAction(MotionEvent.ACTION_CANCEL);
        else
            if(event.getAction() == ACTION_FAKE) {
                event.setAction(MotionEvent.ACTION_UP);
            }
        else if(result) {
                event.setAction(MotionEvent.ACTION_CANCEL);
            }
        else if(this.tapIndicator) {
                event.setAction(MotionEvent.ACTION_DOWN);
                this.tapIndicator = false;
            }
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        final float xDistance = Math.abs(e1.getX() - e2.getX());
        final float yDistance = Math.abs(e1.getY() - e2.getY());
        boolean result = false;

        if(xDistance>this.swipe_Min_Distance)
        {
            if(e1.getX()>e2.getX()) //right to left
            {
                listener.swipe(SWIPE_RIGHT);
            }
            else
            {
                listener.swipe(SWIPE_LEFT);
            }
            result = true;
        }

        return result;
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        this.tapIndicator = true;
        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        return true;
    }

    public static interface SimpleGestureListener
    {
        void swipe(int direction);
        void onDoubleTap();
    }
}
