package com.milezero.mobile.syncUtility.adapter;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.milezero.mobile.api.ApiEndpointInterface;
import com.milezero.mobile.db.realm.RealmSingleton;
import com.milezero.mobile.model.driveritinerary.db.DriverItinerary;
import com.milezero.mobile.utils.Config;
import com.milezero.mobile.utils.L;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import java.io.IOException;


import io.realm.Realm;
import io.realm.RealmObject;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Suraj Dubey on 15/12/15.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

    Realm realm;
    Context mContext;

    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {

        mContext = getContext();
        L.e("call started");
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();

        OkHttpClient client = new OkHttpClient();
        client.interceptors().add(new Interceptor() {
            @Override
            public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Request newRequest = request.newBuilder()
                        .addHeader("X-MZ-AUTH-API-KEY", "eaze1234")
                        .build();

                return chain.proceed(newRequest);
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();


        //prepare call
        ApiEndpointInterface endpointInterface = retrofit.create(ApiEndpointInterface.class);
//        Call<DriverItinerary> call = endpointInterface.getDriverItinerary("milezero-job_kjehkdheekj");
        Call<DriverItinerary> call = endpointInterface.getDriverItineraryDummy();

        call.enqueue(new Callback<DriverItinerary>() {
            @Override
            public void onResponse(Response<DriverItinerary> response, Retrofit retrofit) {

                if (response.isSuccess()) {
                    DriverItinerary itinerary = response.body();
                    realm = RealmSingleton.getInstance(mContext);
                    L.e("Success response");
                    deleteAllRecord();
                    performRealmTransaction(itinerary);

                } else {
                    L.e("failed");
                    L.e(response.errorBody().toString() + " " + response.raw().toString());
                }

            }

            @Override
            public void onFailure(Throwable t) {
                L.e("Inside failure");
                t.printStackTrace();
            }
        });
    }



    private void performRealmTransaction(final DriverItinerary itinerary) {

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm innerRealm) {
                innerRealm.copyToRealm(itinerary);
            }
        }, new Realm.Transaction.Callback() {
            @Override
            public void onSuccess() {
                L.d("Realm Operation Success");

            }

            @Override
            public void onError(Exception e) {
                L.d("Realm Operation Failed");
                e.printStackTrace();
            }
        });
    }

    private void deleteAllRecord() {
        realm.beginTransaction();
        realm.where(DriverItinerary.class).findAll().clear();
        realm.commitTransaction();
    }


}
