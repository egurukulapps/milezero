package com.milezero.mobile.syncUtility.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.milezero.mobile.syncUtility.adapter.SyncAdapter;
import com.milezero.mobile.utils.L;

/**
 * Created by Suraj Dubey on 15/12/15.
 */
public class SyncService extends Service {
    private static SyncAdapter syncAdapter = null;
    private static final Object syncAdapterLock = new Object();

    /**
     * Thread-safe constructor, creates static {@link SyncAdapter} instance.
    */
    @Override
    public void onCreate() {
        super.onCreate();
        L.d("SyncService Created");

        synchronized (syncAdapterLock)
        {
            if(syncAdapter == null)
            {
                syncAdapter = new SyncAdapter(getApplicationContext(), true);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return syncAdapter.getSyncAdapterBinder();
    }
}
