package com.milezero.mobile.syncUtility.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.milezero.mobile.syncUtility.authenticator.SyncAccountAuthenticator;

/**
 * Created by Suraj Dubey on 15/12/15.
 */
public class AuthenticatorService extends Service {
    private SyncAccountAuthenticator authenticator;

    @Override
    public void onCreate() {
        authenticator = new SyncAccountAuthenticator(this);
    }

    /*
    * When the system binds to this Service to make the RPC call
    * return the authenticator’s IBinder.
    */
    @Override
    public IBinder onBind(Intent intent) {
        return authenticator.getIBinder();
    }
}
