package com.milezero.mobile.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.milezero.mobile.R;
import com.milezero.mobile.adapter.recyclerViewAdapter.TaskListRecyclerViewAdapter;
import com.milezero.mobile.api.ApiEndpointInterface;
import com.milezero.mobile.db.realm.RealmSingleton;
import com.milezero.mobile.model.driveritinerary.db.DriverItinerary;
import com.milezero.mobile.model.driveritinerary.db.Stop;
import com.milezero.mobile.service.LocationService;
import com.milezero.mobile.utils.Config;
import com.milezero.mobile.utils.L;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by nishant on 18/2/16.
 */
public class RouteListFragment extends Fragment {


    Context mContext;
    Realm realm;
    RealmResults<DriverItinerary> itineraries;
    RecyclerView recyclerViewRouteList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_route_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerViewRouteList = (RecyclerView) view.findViewById(R.id.recyclerViewRouteList);
        startLocationService();
        getData();


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private void getData() {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();


        //prepare call
        ApiEndpointInterface endpointInterface = retrofit.create(ApiEndpointInterface.class);
//        Call<DriverItinerary> call = endpointInterface.getDriverItinerary("milezero-job_kjehkdheekj");
        Call<DriverItinerary> call = endpointInterface.getDriverItineraryDummy();

        call.enqueue(new Callback<DriverItinerary>() {
            @Override
            public void onResponse(Response<DriverItinerary> response, Retrofit retrofit) {

                if (response.isSuccess()) {
                    DriverItinerary itinerary = response.body();
                    realm = RealmSingleton.getInstance(mContext);
                    L.e("size = " + itinerary.getStops().size());
                    deleteAllRecord();
                    performRealmTransaction(itinerary);

                } else {
                    L.e("failed");
                    L.e(response.errorBody().toString() + " " + response.raw().toString());
                }

            }

            @Override
            public void onFailure(Throwable t) {
                L.e("Inside failure");
                t.printStackTrace();

//                ProgressDialogUtility.dismissProgressDialog();
            }
        });
    }

    private void fetchDataFromDatabase() {
        realm = RealmSingleton.getInstance(mContext);
        DriverItinerary itinerary;
        ArrayList<String> summaryList = new ArrayList<>();
        ArrayList<String> addressList = new ArrayList<>();

        //query
        RealmQuery<DriverItinerary> query = realm.where(DriverItinerary.class);

        L.d(query.count() + " " + "Count");
        //condition: find all
        itineraries = query.findAll();
        int querySize = itineraries.size();

        L.d("Query Size: " + querySize);

        if (querySize > 0) {
            int index = 0;
            itinerary = itineraries.get(index);
            summaryList.add(itinerary.getDriverExternalId());
            addressList.add(itinerary.getId());

            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);

            L.e("stops size " + itinerary.getStops().size());

            RealmList<Stop> stops = manipulateListWithExecutionWindow(itinerary);

            Realm realm = RealmSingleton.getInstance(mContext);
            realm.beginTransaction();
            itinerary.setStops(stops);
            realm.commitTransaction();

//            RouteListRecyclerViewAdapter adapter = new RouteListRecyclerViewAdapter(itinerary, mContext);
//            recyclerViewRouteList.setHasFixedSize(true);
            recyclerViewRouteList.setLayoutManager(layoutManager);


            TaskListRecyclerViewAdapter adapter = new TaskListRecyclerViewAdapter(itinerary, itinerary.getStops(), mContext);

            recyclerViewRouteList.setAdapter(adapter);
        }
    }


    private RealmList<Stop> manipulateListWithExecutionWindow(DriverItinerary driverItinerary) {

        RealmList<Stop> stops = new RealmList<>();
        Realm realm = RealmSingleton.getInstance(mContext);

        realm.beginTransaction();

        for (int j = 0; j < Config.TIME_SLOTS.length; j++) {

            int slotTime = Config.TIME_SLOTS[j];

            RealmList<Stop> smallStops = new RealmList<>();
            for (int i = 0; i < driverItinerary.getStops().size(); i++) {Stop stop = driverItinerary.getStops().get(i);
                String time = stop.getTasks().get(0).getExecutionWindow().getEnd();

                SimpleDateFormat sdf = new SimpleDateFormat(Config.END_TIME_FORMAT);

                try {
                    Date date = sdf.parse(time);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);


                    int hourTime = calendar.get(Calendar.HOUR_OF_DAY);


                    if (hourTime <= slotTime && hourTime > slotTime - 1) {
                        stop.setType(1);

                        stop.setStopNumber(i + 1);
                        smallStops.add(stop);
                        L.e(hourTime + "");

                    }
                } catch (ParseException e) {

                    e.printStackTrace();
                }


            }

            if (smallStops.size() > 0) {
                Stop stopSlot = realm.createObject(Stop.class);

                stopSlot.setType(0);
                stopSlot.setTime(Config.TIME_STRING_SLOTS[j]);
                stops.add(stopSlot);

                stops.addAll(smallStops);
            }


        }

        L.e("size driveritenary= " + driverItinerary.getStops().size() + "  new stops size = " + stops.size());
        realm.commitTransaction();
        return stops;

    }


    private void startLocationService() {
        Intent intent = new Intent(mContext, LocationService.class);
        getActivity().startService(intent);
    }


    private void setGetMinMaxTime() {

    }


    private void performRealmTransaction(final DriverItinerary itinerary) {


        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm innerRealm) {
                innerRealm.copyToRealm(itinerary);
            }
        }, new Realm.Transaction.Callback() {
            @Override
            public void onSuccess() {
                L.d("Realm Operation Success");
                fetchDataFromDatabase();

            }

            @Override
            public void onError(Exception e) {
                L.d("Realm Operation Failed");
                e.printStackTrace();
            }
        });
    }

    private void deleteAllRecord() {
        realm.beginTransaction();
        realm.where(DriverItinerary.class).findAll().clear();
        realm.commitTransaction();

    }

}
