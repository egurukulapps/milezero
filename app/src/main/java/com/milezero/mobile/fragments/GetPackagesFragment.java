package com.milezero.mobile.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.milezero.mobile.R;
import com.milezero.mobile.activities.ContainerLayout;
import com.milezero.mobile.activities.PaymentActivity;
import com.milezero.mobile.adapter.recyclerViewAdapter.PayloadAdapter;
import com.milezero.mobile.db.realm.RealmSingleton;
import com.milezero.mobile.interfaces.OnItemClickListener;
import com.milezero.mobile.model.driveritinerary.db.Address;
import com.milezero.mobile.model.driveritinerary.db.DriverItinerary;
import com.milezero.mobile.model.driveritinerary.db.Payload;
import com.milezero.mobile.model.driveritinerary.db.Stop;
import com.milezero.mobile.model.driveritinerary.db.Task;
import com.milezero.mobile.utils.L;
import com.milezero.mobile.utils.Validation;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by nishant on 10/1/16.
 */


public class GetPackagesFragment extends Fragment {


    DriverItinerary itinerary;

    Realm realm;

    int currentStopIndex;
    ContainerLayout mContext;

    TextView tvAddressLine1, itemCount;
    RealmList<Payload> payloads;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_get_packages, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        tvAddressLine1 = (TextView) view.findViewById(R.id.tvAddressLine1);
        itemCount = (TextView) view.findViewById(R.id.itemCount);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);


        view.findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mContext.manageFragments("collect", null);
//                Intent intent = new Intent(mContext, PaymentActivity.class);
//                intent.putExtra("currentStopIndex", currentStopIndex);
//                mContext.startActivity(intent);

                mContext.manageFragments("arrival_confirm", null);

            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        getIntentData();
        setIntentData();
        payloads = getPayLoad();
        setTaskStopData();


        if (payloads != null) {
            PayloadAdapter payloadAdapter = new PayloadAdapter(payloads, mContext, new OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    mContext.manageFragments("package_view_fragment", null);
                }
            });

            recyclerView.setAdapter(payloadAdapter);
        }


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (ContainerLayout) context;
    }

    private void getIntentData() {
        Intent intent = getActivity().getIntent();

        if (intent != null) {
            currentStopIndex = intent.getIntExtra("currentStopIndex", 0);

        }
    }


    private void setIntentData() {


        realm = RealmSingleton.getInstance(mContext);
        RealmQuery<DriverItinerary> query = realm.where(DriverItinerary.class);

        RealmResults<DriverItinerary> itineraries = query.findAll();
        itinerary = itineraries.get(0);


    }


    private RealmList<Payload> getPayLoad() {

        Task task = itinerary.getStops().get(currentStopIndex).getTasks().get(1);

        if (task.getHasPayload()) {
            RealmList<Payload> payloadRealmList = task.getPayload();

            L.e(payloadRealmList.size() + "   " + payloadRealmList.get(0).getItem_dimension());
            return payloadRealmList;
        }

        L.e("pay load null");
        return null;


    }


    private void setTaskStopData() {

        Stop currentStop = itinerary.getStops().get(currentStopIndex);


        Address address = currentStop.getAddress();

        String street = address.getStreet();
        String unit = address.getUnit();

        String postalCode = address.getPostalCode();


        String addressString = "";
        if (Validation.isValidString(street)) {

            addressString += "\n" + street;
        }
        if (Validation.isValidString(unit)) {

            addressString += "\n" + unit;
        }

        addressString += "\n" + postalCode;
        tvAddressLine1.setText(addressString);

        if (payloads != null) {
            itemCount.setText(payloads.size() + " Parcels");
        }


    }
}
