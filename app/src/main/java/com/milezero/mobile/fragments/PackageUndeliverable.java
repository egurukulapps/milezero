package com.milezero.mobile.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.milezero.mobile.R;
import com.milezero.mobile.activities.ContainerLayout;

/**
 * Created by nishant on 11/1/16.
 */
public class PackageUndeliverable extends Fragment {

    ContainerLayout mContext;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_underliverable, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        view.findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });




    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = (ContainerLayout) context;
    }

}
