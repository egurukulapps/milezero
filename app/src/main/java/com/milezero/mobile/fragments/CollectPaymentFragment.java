package com.milezero.mobile.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.milezero.mobile.R;
import com.milezero.mobile.activities.ContainerLayout;
import com.milezero.mobile.activities.PaymentActivity;

/**
 * Created by nishant on 13/1/16.
 */
public class CollectPaymentFragment extends Fragment {


    PaymentActivity mContext;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_payment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.onContinue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.manageFragments("collected", null);
            }
        });


        view.findViewById(R.id.onChange).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.manageFragments("change", null);
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        mContext.setTitle("Collect Payment");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = (PaymentActivity) context;
    }
}
