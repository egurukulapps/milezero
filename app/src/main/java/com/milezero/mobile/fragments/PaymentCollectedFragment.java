package com.milezero.mobile.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.milezero.mobile.EnvConstants;
import com.milezero.mobile.R;
import com.milezero.mobile.activities.PaymentActivity;
import com.milezero.mobile.api.ApiEndpointInterface;
import com.milezero.mobile.db.realm.RealmSingleton;
import com.milezero.mobile.model.driveritinerary.db.DriverItinerary;
import com.milezero.mobile.model.driveritinerary.db.Task;
import com.milezero.mobile.utils.Config;
import com.milezero.mobile.utils.L;
import com.milezero.mobile.utils.ProgressDialogUtility;
import com.milezero.mobile.utils.ShowToast;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by nishant on 13/1/16.
 */
public class PaymentCollectedFragment extends Fragment {


    PaymentActivity mContext;

    Task updateTaskStatus;

    DriverItinerary itinerary;

    Realm realm;

    int currentStopIndex;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_payment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.editCash).setVisibility(View.GONE);
        view.findViewById(R.id.editCredit).setVisibility(View.GONE);
        view.findViewById(R.id.onProblem).setVisibility(View.GONE);


        TextView tvPayment = (TextView) view.findViewById(R.id.tvPayment);

        tvPayment.setVisibility(View.VISIBLE);

        tvPayment.setText("Payment collected for\n3 of 3 packages");

        getIntentData();
        setIntentData();
        getPayLoad();

        setOnClick(view);


    }


    private void setOnClick(View view) {

        view.findViewById(R.id.onContinue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realm.beginTransaction();
                updateTaskStatus.setState("COMPLETED");
                realm.commitTransaction();
                updateStatus();

            }
        });
    }

    private void getIntentData() {
        Intent intent = getActivity().getIntent();

        if (intent != null) {
            currentStopIndex = intent.getIntExtra("currentStopIndex", 0);

        }
    }


    private void setIntentData() {


        realm = RealmSingleton.getInstance(mContext);
        RealmQuery<DriverItinerary> query = realm.where(DriverItinerary.class);

        RealmResults<DriverItinerary> itineraries = query.findAll();
        itinerary = itineraries.get(0);


    }


    private void getPayLoad() {

        updateTaskStatus = itinerary.getStops().get(currentStopIndex).getTasks().get(1);


        L.e("pay load null");


    }

    @Override
    public void onResume() {
        super.onResume();
        mContext.setTitle("Payment Collected");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = (PaymentActivity) context;
    }


    private void updateStatus() {

        ProgressDialogUtility.showProgressDialog(mContext, "Updating task");

        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(EnvConstants.BASE_URL__MAIN)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();


        //prepare call
        ApiEndpointInterface endpointInterface = retrofit.create(ApiEndpointInterface.class);
//        Call<DriverItinerary> call = endpointInterface.getDriverItinerary("milezero-job_kjehkdheekj");
        Call<Void> call = endpointInterface.updateTask(updateTaskStatus, itinerary.getDriverExternalId());

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Response<Void> response, Retrofit retrofit) {

                if (response.isSuccess()) {
                    ProgressDialogUtility.dismissProgressDialog();

                    ShowToast.showToast(mContext, "Successfully updated");

                } else {
                    L.e("failed");
                    ProgressDialogUtility.dismissProgressDialog();
                    L.e(response.errorBody().toString() + " " + response.raw().toString());
                }

            }

            @Override
            public void onFailure(Throwable t) {
                L.e("Inside failure");
                t.printStackTrace();

                ProgressDialogUtility.dismissProgressDialog();
            }
        });
    }

}
