package com.milezero.mobile.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.milezero.mobile.R;
import com.milezero.mobile.activities.ContainerLayout;
import com.milezero.mobile.activities.PaymentActivity;

/**
 * Created by nishant on 13/1/16.
 */
public class DisburseChangesFragment extends Fragment {

    PaymentActivity mContext;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_payment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.editCash).setVisibility(View.GONE);
        view.findViewById(R.id.editCredit).setVisibility(View.GONE);
        view.findViewById(R.id.onProblem).setVisibility(View.GONE);
        view.findViewById(R.id.tvPayment).setVisibility(View.VISIBLE);
        view.findViewById(R.id.tvChange).setVisibility(View.VISIBLE);
        TextView tvPayment = (TextView) view.findViewById(R.id.tvPayment);

        tvPayment.setVisibility(View.VISIBLE);

        tvPayment.setText("Change Amount");

        view.findViewById(R.id.onContinue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.manageFragments("collected", null);
            }
        });




    }

    @Override
    public void onResume() {
        super.onResume();
        mContext.setTitle("Disburse Change");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = (PaymentActivity) context;
    }
}
