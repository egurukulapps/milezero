package com.milezero.mobile.controlers;

import android.content.Context;

import com.milezero.mobile.db.realm.RealmSingleton;
import com.milezero.mobile.model.driveritinerary.db.DriverItinerary;
import com.milezero.mobile.utils.L;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by nishant on 27/1/16.
 */
public class RealmManager {


    private RealmResults<DriverItinerary> getDriverItinerary(Context mContext) {
        Realm realm = RealmSingleton.getInstance(mContext);

        //query
        RealmQuery<DriverItinerary> query = realm.where(DriverItinerary.class);

        L.d(query.count() + " " + "Count");
        //condition: find all

        return query.findAll();


    }

    private void removeDriverItinerary(Context mContext) {
        Realm realm = RealmSingleton.getInstance(mContext);

        realm.beginTransaction();
        //query
        RealmResults<DriverItinerary> query = realm.where(DriverItinerary.class).findAll();

        query.clear();


    }


}
