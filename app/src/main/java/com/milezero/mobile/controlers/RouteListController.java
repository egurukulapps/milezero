package com.milezero.mobile.controlers;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.milezero.mobile.R;
import com.milezero.mobile.adapter.recyclerViewAdapter.TaskListRecyclerViewAdapter;
import com.milezero.mobile.db.realm.RealmSingleton;
import com.milezero.mobile.model.driveritinerary.db.DriverItinerary;
import com.milezero.mobile.model.driveritinerary.db.Stop;
import com.milezero.mobile.utils.Config;
import com.milezero.mobile.utils.L;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by nishant on 25/1/16.
 */
public class RouteListController {

    private RealmList<Stop> manipulateListWithExecutionWindow(DriverItinerary driverItinerary, Context mContext) {

        RealmList<Stop> stops = new RealmList<>();
        Realm realm = RealmSingleton.getInstance(mContext);

        realm.beginTransaction();

        for (int j = 0; j < Config.TIME_SLOTS.length; j++) {

            int slotTime = Config.TIME_SLOTS[j];

            RealmList<Stop> smallStops = new RealmList<>();
            for (int i = 0; i < driverItinerary.getStops().size(); i++) {

                Stop stop = driverItinerary.getStops().get(i);
                String time = stop.getTasks().get(0).getExecutionWindow().getEnd();

                SimpleDateFormat sdf = new SimpleDateFormat(Config.END_TIME_FORMAT);

                try {
                    Date date = sdf.parse(time);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);


                    int hourTime = calendar.get(Calendar.HOUR_OF_DAY);


                    if (hourTime <= slotTime && hourTime > slotTime - 1) {
                        stop.setType(0);

                        stop.setStopNumber(i + 1);
                        smallStops.add(stop);
                        L.e(hourTime + "");

                    }
                } catch (ParseException e) {

                    e.printStackTrace();
                }


            }

            if (smallStops.size() > 0) {
                Stop stopSlot = realm.createObject(Stop.class);

                stopSlot.setType(1);
                stopSlot.setTime(Config.TIME_STRING_SLOTS[j]);
                stops.add(stopSlot);

                stops.addAll(smallStops);
            }


        }

        L.e("size driveritenary= " + driverItinerary.getStops().size() + "  new stops size = " + stops.size());
        realm.commitTransaction();
        return stops;

    }

    private void fetchDataFromDatabase(Context mContext, Realm realm, RealmResults<DriverItinerary> itineraries, View view) {
        realm = RealmSingleton.getInstance(mContext);
        DriverItinerary itinerary;
        ArrayList<String> summaryList = new ArrayList<>();
        ArrayList<String> addressList = new ArrayList<>();

        //query
        RealmQuery<DriverItinerary> query = realm.where(DriverItinerary.class);

        L.d(query.count() + " " + "Count");
        //condition: find all
        itineraries = query.findAll();
        int querySize = itineraries.size();

        L.d("Query Size: " + querySize);

        if (querySize > 0) {
            int index = 0;
            itinerary = itineraries.get(index);
            summaryList.add(itinerary.getDriverExternalId());
            addressList.add(itinerary.getId());

            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);

            L.e("stops size " + itinerary.getStops().size());

            RealmList<Stop> stops = manipulateListWithExecutionWindow(itinerary, mContext);

            realm.beginTransaction();
            itinerary.setStops(stops);
            realm.commitTransaction();
            RecyclerView recyclerViewRouteList = (RecyclerView) view.findViewById(R.id.recyclerViewRouteList);
//            RouteListRecyclerViewAdapter adapter = new RouteListRecyclerViewAdapter(itinerary, mContext);
//            recyclerViewRouteList.setHasFixedSize(true);
            recyclerViewRouteList.setLayoutManager(layoutManager);


            TaskListRecyclerViewAdapter adapter = new TaskListRecyclerViewAdapter(itinerary, itinerary.getStops(), mContext);

            recyclerViewRouteList.setAdapter(adapter);
        }
    }
}
