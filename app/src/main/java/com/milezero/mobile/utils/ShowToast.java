package com.milezero.mobile.utils;

import android.content.Context;
import android.widget.Toast;

import com.milezero.mobile.R;

/**
 * Created by nishant on 30/12/15.
 */
public class ShowToast {

    public static void showToastError(Context context) {
        Toast.makeText(context, context.getResources().getString(R.string.gen_error), Toast.LENGTH_SHORT).show();
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
