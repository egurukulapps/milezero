package com.milezero.mobile.utils;

import com.milezero.mobile.EnvConstants;

/**
 * Created by suraj on 9/12/15.
 */
public class Config {

    public static String BASE_URL = EnvConstants.BASE_URL;
    public static final String END_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String GOOGLE_API_KEY = EnvConstants.GOOGLE_API_KEY;
    public static final String GOOGLE_API_KEY_DIRECTION = EnvConstants.GOOGLE_API_KEY_DIRECTION;
    public static final boolean IS_DEBUG = EnvConstants.IS_DEBUG;
    public static final int MAPS_ZOOM_IN_LEVEL = 15;
    public static final int MAPS_ZOOM_IN_LEVEL_EXTREME = 19;

    public static final int[] TIME_SLOTS = {10, 11, 12, 13, 14, 15, 16, 17, 18};
    public static final String[] TIME_STRING_SLOTS = {"9 am - 10 am", "10 am - 11 am", "11 am - 12 pm", "12 pm - 1pm", "1 pm - 2 pm", "2 pm - 3 pm", "3 pm - 4 pm", "4 pm - 5 pm", "5 pm - 6 pm"};


}
