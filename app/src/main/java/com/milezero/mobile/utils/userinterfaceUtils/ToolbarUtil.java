package com.milezero.mobile.utils.userinterfaceUtils;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.milezero.mobile.R;

/**
 * Created by Suraj Dubey on 26/02/16.
 */
public class ToolbarUtil {

    static Toolbar toolbar;

    public static void setToolbar(Context context)
    {
        Activity activity = (Activity) context;
        ((AppCompatActivity)activity).setSupportActionBar(toolbar);

        ActionBar bar = ((AppCompatActivity)activity).getSupportActionBar();
        if(bar!=null)
        {
            bar.setDisplayShowTitleEnabled(false);
            bar.setDisplayHomeAsUpEnabled(true);
            bar.setDisplayShowHomeEnabled(true);
        }
    }
}
