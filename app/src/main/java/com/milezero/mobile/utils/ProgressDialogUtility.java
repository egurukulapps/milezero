package com.milezero.mobile.utils;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by suraj on 9/12/15.
 */
public class ProgressDialogUtility {

    static ProgressDialog mProgressDialog;

    public static void showProgressDialog(Context context, String message)
    {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.show();
    }

    public static void dismissProgressDialog()
    {
        if(mProgressDialog!=null && mProgressDialog.isShowing())
        {
            mProgressDialog.dismiss();
        }
    }
}
