package com.milezero.mobile.model.messages;

import io.realm.RealmObject;

/**
 * Created by nishant on 16/1/16.
 */
public class Messages extends RealmObject {

    private String message, time;

    private boolean isSelf;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isSelf() {
        return isSelf;
    }

    public void setIsSelf(boolean isSelf) {
        this.isSelf = isSelf;
    }
}
