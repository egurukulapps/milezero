package com.milezero.mobile.model.driveritinerary.db;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Suraj Dubey on 11/12/15.
 */
public class DriverItinerary extends RealmObject {
    private String id;

    public String getId() { return this.id; }

    public void setId(String id) { this.id = id; }

    private String driverExternalId;

    public String getDriverExternalId() { return this.driverExternalId; }

    public void setDriverExternalId(String driverExternalId) { this.driverExternalId = driverExternalId; }

    private RealmList<Stop> stops;

    public RealmList<Stop> getStops() { return this.stops; }

    public void setStops(RealmList<Stop> stops) { this.stops = stops; }
}
