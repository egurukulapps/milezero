package com.milezero.mobile.model.driveritinerary.db;

import io.realm.RealmObject;

/**
 * Created by Suraj Dubey on 11/12/15.
 */
public class Payload extends RealmObject {
    private String externalId;
    private String item_number;
    private String item_name;

    public String getItem_dimension() {
        return item_dimension;
    }

    public void setItem_dimension(String item_dimension) {
        this.item_dimension = item_dimension;
    }

    public String getItem_number() {
        return item_number;
    }

    public void setItem_number(String item_number) {
        this.item_number = item_number;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    private String item_dimension;

    public String getExternalId() { return this.externalId; }

    public void setExternalId(String externalId) { this.externalId = externalId; }

    private String sku;

    public String getSku() { return this.sku; }

    public void setSku(String sku) { this.sku = sku; }

    private Geocode2 geocode;

    public Geocode2 getGeocode() { return this.geocode; }

    public void setGeocode(Geocode2 geocode) { this.geocode = geocode; }

    private String expiresAt;

    public String getExpiresAt() { return this.expiresAt; }

    public void setExpiresAt(String expiresAt) { this.expiresAt = expiresAt; }
}

