package com.milezero.mobile.model.driveritinerary.network;

import java.util.ArrayList;

/**
 * Created by Suraj Dubey on 11/12/15.
 */

public class Task
{
    private int step;

    public int getStep() { return this.step; }

    public void setStep(int step) { this.step = step; }

    private ExecutionWindow executionWindow;

    public ExecutionWindow getExecutionWindow() { return this.executionWindow; }

    public void setExecutionWindow(ExecutionWindow executionWindow) { this.executionWindow = executionWindow; }

    private String taskId;

    public String getTaskId() { return this.taskId; }

    public void setTaskId(String taskId) { this.taskId = taskId; }

    private String taskName;

    public String getTaskName() { return this.taskName; }

    public void setTaskName(String taskName) { this.taskName = taskName; }

    private String description;

    public String getDescription() { return this.description; }

    public void setDescription(String description) { this.description = description; }

    private String driverExternalId;

    public String getDriverExternalId() { return this.driverExternalId; }

    public void setDriverExternalId(String driverExternalId) { this.driverExternalId = driverExternalId; }

    private boolean hasPayload;

    public boolean getHasPayload() { return this.hasPayload; }

    public void setHasPayload(boolean hasPayload) { this.hasPayload = hasPayload; }

    private ArrayList<Payload> payload;

    public ArrayList<Payload> getPayload() { return this.payload; }

    public void setPayload(ArrayList<Payload> payload) { this.payload = payload; }

    private String createdAt;

    public String getCreatedAt() { return this.createdAt; }

    public void setCreatedAt(String createdAt) { this.createdAt = createdAt; }

    private Geocode3 geocode;

    public Geocode3 getGeocode() { return this.geocode; }

    public void setGeocode(Geocode3 geocode) { this.geocode = geocode; }
}