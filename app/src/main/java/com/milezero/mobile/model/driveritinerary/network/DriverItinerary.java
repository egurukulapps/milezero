package com.milezero.mobile.model.driveritinerary.network;

import java.util.ArrayList;

/**
 * Created by Suraj Dubey on 11/12/15.
 */
public class DriverItinerary {
    private String id;

    public String getId() { return this.id; }

    public void setId(String id) { this.id = id; }

    private String driverExternalId;

    public String getDriverExternalId() { return this.driverExternalId; }

    public void setDriverExternalId(String driverExternalId) { this.driverExternalId = driverExternalId; }

    private ArrayList<Stop> stops;

    public ArrayList<Stop> getStops() { return this.stops; }

    public void setStops(ArrayList<Stop> stops) { this.stops = stops; }
}
