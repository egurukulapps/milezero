package com.milezero.mobile.model.driveritinerary.db;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Suraj Dubey on 11/12/15.
 */

public class Task extends RealmObject
{
    private int step;

    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getStep() { return this.step; }

    public void setStep(int step) { this.step = step; }

    private ExecutionWindow executionWindow;

    public ExecutionWindow getExecutionWindow() { return this.executionWindow; }

    public void setExecutionWindow(ExecutionWindow executionWindow) { this.executionWindow = executionWindow; }

    private String taskId;

    public String getTaskId() { return this.taskId; }

    public void setTaskId(String taskId) { this.taskId = taskId; }

    private String taskName;

    public String getTaskName() { return this.taskName; }

    public void setTaskName(String taskName) { this.taskName = taskName; }

    private String description;

    public String getDescription() { return this.description; }

    public void setDescription(String description) { this.description = description; }

    private String driverExternalId;

    public String getDriverExternalId() { return this.driverExternalId; }

    public void setDriverExternalId(String driverExternalId) { this.driverExternalId = driverExternalId; }

    private boolean hasPayload;

    public boolean getHasPayload() { return this.hasPayload; }

    public void setHasPayload(boolean hasPayload) { this.hasPayload = hasPayload; }

    private RealmList<Payload> payload;

    public RealmList<Payload> getPayload() { return this.payload; }

    public void setPayload(RealmList<Payload> payload) { this.payload = payload; }

    private String createdAt;

    public String getCreatedAt() { return this.createdAt; }

    public void setCreatedAt(String createdAt) { this.createdAt = createdAt; }

    private Geocode3 geocode;

    public Geocode3 getGeocode() { return this.geocode; }

    public void setGeocode(Geocode3 geocode) { this.geocode = geocode; }
}