package com.milezero.mobile.model.driveritinerary.db;

import io.realm.RealmObject;

/**
 * Created by harmeetservesy on 11/12/15.
 */
public class ExecutionWindow extends RealmObject {
    private String start;
    private String end;

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
}
