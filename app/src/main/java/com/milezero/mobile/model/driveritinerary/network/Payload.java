package com.milezero.mobile.model.driveritinerary.network;

/**
 * Created by Suraj Dubey on 11/12/15.
 */
public class Payload
{
    private String externalId;

    public String getExternalId() { return this.externalId; }

    public void setExternalId(String externalId) { this.externalId = externalId; }

    private String sku;

    public String getSku() { return this.sku; }

    public void setSku(String sku) { this.sku = sku; }

    private Geocode2 geocode;

    public Geocode2 getGeocode() { return this.geocode; }

    public void setGeocode(Geocode2 geocode) { this.geocode = geocode; }

    private String expiresAt;

    public String getExpiresAt() { return this.expiresAt; }

    public void setExpiresAt(String expiresAt) { this.expiresAt = expiresAt; }
}

