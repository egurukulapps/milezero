package com.milezero.mobile.model.driveritinerary.db;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Suraj Dubey on 11/12/15.
 */
public class Stop extends RealmObject {
    private Geocode geocode;

    public Geocode getGeocode() { return this.geocode; }

    public void setGeocode(Geocode geocode) { this.geocode = geocode; }

    private Address address;

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Address getAddress() { return this.address; }

    public void setAddress(Address address) { this.address = address; }

    private Contact contact;

    private int type;

    public int getStopNumber() {
        return stopNumber;
    }

    public void setStopNumber(int stopNumber) {
        this.stopNumber = stopNumber;
    }

    private int stopNumber;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

   private String time;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Contact getContact() { return this.contact; }

    public void setContact(Contact contact) { this.contact = contact; }

    private String notes;

    public String getNotes() { return this.notes; }

    public void setNotes(String notes) { this.notes = notes; }

    private RealmList<Task> tasks;

    public RealmList<Task> getTasks() { return this.tasks; }

    public void setTasks(RealmList<Task> tasks) { this.tasks = tasks; }
}
