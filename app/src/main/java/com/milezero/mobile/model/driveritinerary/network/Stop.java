package com.milezero.mobile.model.driveritinerary.network;

import java.util.ArrayList;

/**
 * Created by Suraj Dubey on 11/12/15.
 */
public class Stop {
    private Geocode geocode;

    public Geocode getGeocode() { return this.geocode; }

    public void setGeocode(Geocode geocode) { this.geocode = geocode; }

    private Address address;

    public Address getAddress() { return this.address; }

    public void setAddress(Address address) { this.address = address; }

    private Contact contact;

    public Contact getContact() { return this.contact; }

    public void setContact(Contact contact) { this.contact = contact; }

    private String notes;

    public String getNotes() { return this.notes; }

    public void setNotes(String notes) { this.notes = notes; }

    private ArrayList<Task> tasks;

    public ArrayList<Task> getTasks() { return this.tasks; }

    public void setTasks(ArrayList<Task> tasks) { this.tasks = tasks; }
}
