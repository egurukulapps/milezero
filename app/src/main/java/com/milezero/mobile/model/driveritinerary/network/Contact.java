package com.milezero.mobile.model.driveritinerary.network;

/**
 * Created by Suraj Dubey on 11/12/15.
 */

public class Contact
{
    private String externalId;

    public String getExternalId() { return this.externalId; }

    public void setExternalId(String externalId) { this.externalId = externalId; }

    private String name;

    public String getName() { return this.name; }

    public void setName(String name) { this.name = name; }

    private String email;

    public String getEmail() { return this.email; }

    public void setEmail(String email) { this.email = email; }

    private String phone;

    public String getPhone() { return this.phone; }

    public void setPhone(String phone) { this.phone = phone; }

    private String profileImageUrl;

    public String getProfileImageUrl() { return this.profileImageUrl; }

    public void setProfileImageUrl(String profileImageUrl) { this.profileImageUrl = profileImageUrl; }
}