package com.milezero.mobile.model.driveritinerary.db;

import io.realm.RealmObject;

/**
 * Created by Suraj Dubey on 11/12/15.
 */

public class Address extends RealmObject {
    private String street;

    public String getStreet() { return this.street; }

    public void setStreet(String street) { this.street = street; }

    private String unit;

    public String getUnit() { return this.unit; }

    public void setUnit(String unit) { this.unit = unit; }

    private String city;

    public String getCity() { return this.city; }

    public void setCity(String city) { this.city = city; }

    private String state;

    public String getState() { return this.state; }

    public void setState(String state) { this.state = state; }

    private String countryCode;

    public String getCountryCode() { return this.countryCode; }

    public void setCountryCode(String countryCode) { this.countryCode = countryCode; }

    private String postalCode;

    public String getPostalCode() { return this.postalCode; }

    public void setPostalCode(String postalCode) { this.postalCode = postalCode; }

    private String company;

    public String getCompany() { return this.company; }

    public void setCompany(String company) { this.company = company; }

    private String addressNotes;

    public String getAddressNotes() { return this.addressNotes; }

    public void setAddressNotes(String addressNotes) { this.addressNotes = addressNotes; }

    private String accessDetails;

    public String getAccessDetails() { return this.accessDetails; }

    public void setAccessDetails(String accessDetails) { this.accessDetails = accessDetails; }

    private String addressType;

    public String getAddressType() { return this.addressType; }

    public void setAddressType(String addressType) { this.addressType = addressType; }
}
