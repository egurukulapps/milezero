package com.milezero.mobile.model.temp;

import com.milezero.mobile.model.driveritinerary.db.Task;

/**
 * Created by Suraj Dubey on 18/12/15.
 */
public class TaskData {
    private Task task;
    private int taskId;
    private int stopId;

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public int getStopId() {
        return stopId;
    }

    public void setStopId(int stopId) {
        this.stopId = stopId;
    }
}
