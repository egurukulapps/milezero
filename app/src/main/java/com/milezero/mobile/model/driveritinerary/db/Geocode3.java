package com.milezero.mobile.model.driveritinerary.db;

import io.realm.RealmObject;

/**
 * Created by Suraj Dubey on 11/12/15.
 */
public class Geocode3 extends RealmObject {
    private double latitude;

    public double getLatitude() { return this.latitude; }

    public void setLatitude(double latitude) { this.latitude = latitude; }

    private double longitude;

    public double getLongitude() { return this.longitude; }

    public void setLongitude(double longitude) { this.longitude = longitude; }
}
