package com.milezero.mobile;

public class EnvConstants {

    public static String BASE_URL = "http://private-e08d7-milezeropreprod.apiary-mock.com";
    public static boolean IS_DEBUG = false;

    public static final String CONTENT_AUTHORITY = "com.milezero.mobile.datasync.provider";
}
